import { GoogleConstants } from 'src/auth/constants/provider-constants';

export const InviteUrl = (id) => {
  return GoogleConstants.hostUrl + id;
};
