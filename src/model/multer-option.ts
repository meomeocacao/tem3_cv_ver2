import { HttpException, HttpStatus } from '@nestjs/common';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuid } from 'uuid';
import fs from 'fs';
import { unlink } from 'fs/promises';
import { GoogleConstants } from 'src/auth/constants/provider-constants';
export enum DestType {
  User = 'uploads/user-avatar/',
  CV = 'uploads/cv-avatar/',
}
export async function removeImg(imgurl: string) {
  const imgPath = './' + imgurl.slice(GoogleConstants.hostUrl.length);
  console.log(imgPath);
  if (fs.existsSync(imgPath)) await unlink(imgPath);
  {
    console.log('successfully deleted Old avatar');
  }
}

export const multerOptions = (dest: DestType) => {
  return {
    // Check the mimetypes to allow for upload
    fileFilter: (req: any, file: any, cb: any) => {
      if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
        // Allow storage of file
        cb(null, true);
      } else {
        // Reject file
        cb(
          new HttpException(
            `Unsupported file type ${extname(file.originalname)}`,
            HttpStatus.BAD_REQUEST,
          ),
          false,
        );
      }
    },
    storage: diskStorage({
      destination: './' + dest,
      filename: (req, file, cb) => {
        const filename = `${Date.now()}-${uuid()}${extname(file.originalname)}`;
        cb(null, `${filename}`);
      },
    }),
  };
};
