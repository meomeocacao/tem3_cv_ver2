import * as bcrypt from 'bcrypt';

export const Hashed = async (value: string): Promise<string> => {
  if (value) {
    const salt = await bcrypt.genSalt();
    return await bcrypt.hash(value, salt);
  }
};

export const Compare = async (valueCompare: string, valueToCompare: string) => {
  return await bcrypt.compare(valueCompare, valueToCompare);
};
