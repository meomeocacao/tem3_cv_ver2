import { Gender, DefaultAvaUrl } from 'src/users/constants/user.constant';

export const DefaultAva = (gender: Gender) => {
  if (gender == Gender.Male) {
    return DefaultAvaUrl.Male;
  }
  return DefaultAvaUrl.Female;
};
