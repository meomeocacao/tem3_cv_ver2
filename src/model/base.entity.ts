import { Exclude } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { v4 as uuid } from 'uuid';
export abstract class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  // @Exclude()
  id: string;
  @CreateDateColumn()
  @Exclude()
  createAt: Date;
  @UpdateDateColumn()
  @Exclude()
  updateAt: Date;
  @Column({ default: false })
  @Exclude()
  isDelete: boolean;
}
