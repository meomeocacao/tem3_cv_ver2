import { SetMetadata } from '@nestjs/common';
import { RoleUser } from 'src/users/constants/user.constant';

export const ROLES_KEY = 'roles';
export const Roles = (...roles: RoleUser[]) => {
  return SetMetadata(ROLES_KEY, roles);
};
