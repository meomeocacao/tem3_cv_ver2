import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Question } from 'src/questions/entities/questions.entity';
import { AnswerQuestionsModule } from 'src/answerQuestions/answerQuestion.module';
import { QuestionModule } from 'src/questions/questions.module';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { TopicQuestionModule } from 'src/topicsQuestion/topicQuestion.module';
import { FieldController } from './fields.controller';
import { FieldRepository } from './fields.repository';
import { FieldService } from './fields.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([FieldRepository, Question, TopicQuestion]),
  ],
  controllers: [FieldController],
  providers: [FieldService],
  exports: [FieldService],
})
export class FieldsModule {}
