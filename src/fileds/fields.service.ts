import {
  ConflictException,
  ConsoleLogger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AnswerQuestionsService } from 'src/answerQuestions/answerQuestion.service';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
import { ReturnStatus } from 'src/auth/constants/constants';
import {
  EntityType,
  IsDeleteEntity,
  NameEntityCode,
  ReturnNotification,
} from 'src/constants/entity.constants';
import { ListField } from 'src/constants/field.constant';
import { CreateQuestionDTO } from 'src/questions/dtos/create-question.dto';
import { NewQuestionDTO } from 'src/questions/dtos/update-question.dto';
import { Question } from 'src/questions/entities/questions.entity';
import { QuestionService } from 'src/questions/questions.service';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { TopicQuestionService } from 'src/topicsQuestion/topicQuestion.service';
import { Repository } from 'typeorm';
import { GetFieldRelation, RelationField } from './constants/field.constant';
import { CreateFieldDTO } from './dtos/create-field';
import {
  CreateFieldWithQuestionsDTO,
  CreateQuestionFieldDTO,
} from './dtos/create-field-with-questions';
import { UpdateFieldDTO } from './dtos/update-field';
import { Field } from './entities/field.entity';
import { FieldRepository } from './fields.repository';

export class FieldService {
  constructor(
    @InjectRepository(FieldRepository)
    private fieldRepository: FieldRepository,
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
    @InjectRepository(TopicQuestion)
    private topicQuestionRepo: Repository<TopicQuestion>,
  ) {}

  //   get All
  async getAll(): Promise<Field[]> {
    return await this.fieldRepository.find({
      where: { isDelete: IsDeleteEntity.False },
      relations: RelationField,
    });
  }

  //   create Field with Quest
  async createFieldQuest(
    createDto: CreateFieldWithQuestionsDTO,
  ): Promise<Field> {
    const field = await this.fieldRepository.save(
      this.fieldRepository.create({
        nameField: createDto.nameField,
      }),
    );

    createDto.questions.forEach(async (question) => {
      const topic = await this.topicQuestionRepo.findOne({
        topicQuestion: question.topicQuestion,
      });
      const _question = await this.questionRepository.save(
        this.questionRepository.create({
          contentQuestion: question.contentQuestion,
          field: field,
          topicQuestion: topic,
          titleQuestion: question.titleQuestion,
        }),
      );
      console.log(_question);
    });

    return field;
  }

  // get field with questions by id
  // async getFieldQues(fieldId: string): Promise<Question[]> {
  //   return await this.questionRepository.find({
  //     where: { field: { id: fieldId } },
  //   });
  // }

  // get field by id
  async getFieldById(id: string): Promise<Field> {
    return await this.fieldRepository.findOne({
      where: { id, isDelete: IsDeleteEntity.False },
      relations: GetFieldRelation,
    });
  }

  // get field by id or name
  async queryField(queryString: string): Promise<Field> {
    console.log(queryString);

    return await this.fieldRepository.findFieldByQuery(queryString);
  }

  // //   create just new field
  async createField(fieldDto: CreateFieldDTO): Promise<Field> {
    const fieldCode = NameEntityCode(fieldDto.nameField, EntityType.Field);
    // console.log(fieldCode);
    // const fieldCode = NameEntityCode(fieldDto.nameField, EntityType.Field);
    //check fileld exist
    // const field = await this.fieldRepository.findOne({ where: { fieldCode } });
    // if (field) throw new ConflictException();
    const _field = this.fieldRepository.create({
      nameField: fieldDto.nameField,
      fieldCode,
    });
    return await this.fieldRepository.save(_field);
  }

  // get question by field id
  async getQuestionFromField(fieldId: string): Promise<Question[]> {
    return await this.questionRepository.find({
      where: { field: { id: fieldId } },
    });
  }

  // answer questions geted from field id
  //   async answerQuestionsFromField(
  //     cookie: any,
  //     answers: CreateAnswerQuestion[],
  //   ): Promise<any[]> {}
  // add questions to field by topic
  // async addQuestByTopic(newQuest: NewQuestionDTO) {
  //   const fieldCode = NameFieldCode(newQuest.nameField);
  //   const _field = await this.fieldRepository.findOne({
  //     fieldCode,
  //   });
  //   const _topic = await this.topicQuestionRepo.getTopic(newQuest.topicQuestion);
  //   if (!_field)
  //     throw new NotFoundException(
  //       `Not found Field with name: ${newQuest.nameField}`,
  //     );
  //   await this.questionsService.createQuestionByTopic(
  //     newQuest.contentQuestion,
  //     _field,
  //     _topic,
  //   );
  //   return {
  //     message: ReturnStatus.Success,
  //     field: await this.fieldRepository.findOne({
  //       where: { fieldCode },
  //       relations: ['questions'],
  //     }),
  //   };
  // }

  // //   update field and add question
  // async updateField(id: string, fieldDto: UpdateFieldDTO): Promise<any> {
  //   const questions = await this.questionsService.createArrQuestion(
  //     fieldDto.questions,
  //   );
  //   await this.fieldRepository.update(id, { ...fieldDto, questions });
  //   return {
  //     message: ReturnStatus.Update,
  //     field: await this.fieldRepository.findOne(id),
  //   };
  // }
  //   add questions to field id
  async addQuestionToField(
    id: string,
    _questions: CreateQuestionFieldDTO[],
  ): Promise<Field> {
    //   get field
    const field = await this.fieldRepository.findFieldByQuery(id);
    // const field = await this.getFieldById(id);
    console.log(field);

    if (!field) throw new NotFoundException();
    // const questions = await this.questionsService.createArrQuestion(_questions);
    _questions.forEach(async (question) => {
      return await this.questionRepository.save(
        this.questionRepository.create({
          contentQuestion: question.contentQuestion,
          field,
          topicQuestion: { topicQuestion: question.topicQuestion },
          titleQuestion: question.titleQuestion,
        }),
      );
    });

    // await this.fieldRepository.update(id, { questions: { ...questions } });
    return await this.fieldRepository.findOne({
      where: { id, isDelete: IsDeleteEntity.False },
      relations: GetFieldRelation,
    });
  }

  // remove question in field
  async removeQuestionInField(
    fieldId: string,
    questionId: string,
  ): Promise<string> {
    console.log('vao k ta');

    const field = await this.fieldRepository.findFieldByQuery(fieldId);
    const question = await this.questionRepository.findOne({
      where: { id: questionId, isDelete: IsDeleteEntity.False, field },
    });
    // console.log(question);

    // await this.questionRepository.delete({
    //   id: questionId,
    //   field,
    // });
    // await this.questionRepository.delete({ id: questionId });
    return ReturnStatus.Delete;
  }

  async updateName(queryString: string, newName: string): Promise<Field> {
    const field = await this.fieldRepository.findFieldByQuery(queryString);
    await this.fieldRepository.update(field.id, {
      nameField: newName,
      fieldCode: NameEntityCode(newName, EntityType.Field),
    });
    return await this.fieldRepository.findOne({
      id: field.id,
      isDelete: IsDeleteEntity.False,
    });
  }

  // //   remove field
  async removeField(id: string): Promise<ReturnNotification> {
    await this.fieldRepository.delete(id);
    return {
      message: ReturnStatus.Delete,
      value: id,
    };
  }

  // async getFieldById(id: string): Promise<Field> {
  //   return await this.fieldRepository.findOne(id);
  // }
  // // get questions from field by field id
  // async getQuestionFromField(id: string): Promise<Question[]> {
  //   return await this.questionsService.getQuestionFromField(id);
  // }
  // // get field by fieldcode
  // async getFieldByFieldCode(fieldCode: string): Promise<Field> {
  //   return await this.fieldRepository.findOne({
  //     where: { fieldCode },
  //     relations: RelationField,
  //   });
  // }

  async delete(nameField: string) {
    const fieldCode = NameEntityCode(nameField, EntityType.Field);
    const field = await this.fieldRepository.findOne({ fieldCode });
    if (!field) throw new NotFoundException();
    return this.fieldRepository.update(field.id, {
      nameField: field.nameField + Date.now(),
      isDelete: IsDeleteEntity.True,
      fieldCode: field.fieldCode + Date.now(),
    });
  }
}
