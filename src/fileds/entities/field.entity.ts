import { Exclude } from 'class-transformer';
import { BaseEntity } from 'src/model/base.entity';
import { Resume } from 'src/resumes/entities/resume.entity';
import { getQuestIt, Question } from 'src/questions/entities/questions.entity';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import {
  BeforeInsert,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { v4 as uuid } from 'uuid';
import { CreateFieldWithQuestionsDTO } from '../dtos/create-field-with-questions';
import { EntityType, NameEntityCode } from 'src/constants/entity.constants';
// import { FieldService } from '../fields.service';

@Entity()
export class Field extends BaseEntity {
  // @Column({ type: 'enum', enum: Fields, default: Fields.IT })
  @Column({ unique: true })
  nameField: string;

  // @OneToMany(() => ResumeEntity, (ResumeEntity) => ResumeEntity.field)
  // Resume: ResumeEntity[];

  // @OneToMany(() => TopicQuestion, (topicQuestion) => topicQuestion.fieldEntity)
  // topicQuestion: TopicQuestion[];
  @Column()
  fieldCode: string;

  @OneToMany(() => Resume, (resume) => resume.field, {
    nullable: true,
  })
  resume: Resume;

  @OneToMany(() => Question, (questions) => questions.field, { cascade: true })
  questions: Question[];

  @BeforeInsert()
  generate() {
    this.fieldCode = NameEntityCode(this.nameField, EntityType.Field);
    this.id = uuid();
  }
}
