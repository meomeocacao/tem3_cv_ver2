import {
  EntityType,
  IsDeleteEntity,
  NameEntityCode,
} from 'src/constants/entity.constants';
import { Brackets, EntityRepository, Repository } from 'typeorm';
import { GetFieldRelation, RelationField } from './constants/field.constant';
import { Field } from './entities/field.entity';

@EntityRepository(Field)
export class FieldRepository extends Repository<Field> {
  // find field by name
  async findFieldByName(fieldCode: string): Promise<Field | undefined> {
    return await this.findOne({ fieldCode: fieldCode });
  }
  //   find field by query id or filedCode
  async findFieldByQuery(queryString: string): Promise<Field | undefined> {
    console.log(queryString);

    return await this.createQueryBuilder('field_entity')
      .leftJoinAndSelect('field_entity.questions', 'question')
      .leftJoinAndSelect('question.topicQuestion', 'topic')
      .where('field_entity.isDelete =false')
      .andWhere(
        new Brackets((qb) => {
          qb.where('field_entity.id =:id', { id: queryString }).orWhere(
            'field_entity.fieldCode =:queryString',
            {
              queryString: NameEntityCode(queryString, EntityType.Field),
            },
          );
        }),
      )
      .getOne();

    // return await this.findOne({
    //   where: [
    //     { id: queryString },
    //     { isDelete: IsDeleteEntity.False },
    //     { fieldCode: NameEntityCode(queryString, EntityType.Field) },
    //   ],
    //   relations: RelationField,
    // });
  }
  // select questions by query by id string
  async getFieldQuestion(id: string): Promise<Field> {
    // return await this.createQueryBuilder('field_entity')
    //   .leftJoinAndSelect('field_entity.questions', 'question')
    //   .where('field_entity.id =:id', { id })
    //   .orWhere('field_entity.fieldCode =:id', { id })
    //   .getOne();
    return await this.findOne({
      where: [{ id }, { isDelete: IsDeleteEntity.False }, { fieldCode: id }],
      relations: GetFieldRelation,
    });
  }
}
