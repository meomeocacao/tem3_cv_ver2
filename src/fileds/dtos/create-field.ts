import { ApiConsumes, ApiProperty } from '@nestjs/swagger';
import { CreateQuestionDTO } from 'src/questions/dtos/create-question.dto';
import { Question } from 'src/questions/entities/questions.entity';

export class CreateFieldDTO {
  @ApiProperty({ description: 'Enter name of field', type: 'string' })
  nameField: string;
}
