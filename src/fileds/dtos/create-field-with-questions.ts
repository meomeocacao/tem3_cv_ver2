import { ApiProperty } from '@nestjs/swagger';
import { Topics } from 'src/constants/topic.constant';
import { CreateQuestionDTO } from 'src/questions/dtos/create-question.dto';

export class CreateFieldWithQuestionsDTO {
  @ApiProperty({
    type: 'string',
    default: 'Manager',
    description: 'Name of field to add',
  })
  nameField: string;
  @ApiProperty({
    type: 'array',
    items: {
      type: 'object',
      properties: {
        questions: { type: 'array' },
      },
      default: {
        contentQuestion: 'Cau hoi',
        topicQuestion: Topics.Skills,
        titleQuestion: 'title question',
      },
    },
  })
  questions: CreateQuestionFieldDTO[];
}

export class CreateQuestionFieldDTO {
  @ApiProperty({ type: 'string', default: 'Cau hoi' })
  contentQuestion: string;
  @ApiProperty({ type: 'enum', enum: Topics, default: Topics.Other })
  topicQuestion: string;
  @ApiProperty({ type: 'string', default: 'title question' })
  titleQuestion: string;
}

export class AddQuestionToFieldDTO {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'object',
      properties: {
        questions: { type: 'array' },
      },
      default: {
        contentQuestion: 'Cau hoi',
        topicQuestion: Topics.Skills,
        titleQuestion: 'title question',
      },
    },
  })
  questions: CreateQuestionFieldDTO[];
}
