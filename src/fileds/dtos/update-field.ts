import { ApiProperty } from '@nestjs/swagger';
import { Topics } from 'src/constants/topic.constant';
import { CreateQuestionDTO } from 'src/questions/dtos/create-question.dto';

export class UpdateFieldDTO {
  @ApiProperty({ description: 'Enter name of field to update', type: 'string' })
  nameField: string;
  @ApiProperty({
    type: 'array',
    items: {
      properties: {
        contentQuestion: { type: 'string' },
        topicQuestion: { type: 'enum', enum: [Topics] },
      },
      default: {
        contentQuestion: 'Cau hoi',
        topicQuestion: 'education',
      },
    },
  })
  questions: CreateQuestionDTO[];
}

export class UpdateNameFieldDTO {
  @ApiProperty({
    description: 'Enter name or id of field to update',
    type: 'string',
  })
  queryString: string;
  @ApiProperty({ description: 'Enter new name of field', type: 'string' })
  nameField: string;
}
