import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { Cookies } from 'src/decorators/cookies';
import { CreateFieldDTO } from './dtos/create-field';
import { Field } from './entities/field.entity';
import { FieldService } from './fields.service';
import { ListField } from 'src/constants/field.constant';
import { Question } from 'src/questions/entities/questions.entity';
import { Roles } from 'src/decorators/roles.decorator';
import { RoleUser } from 'src/users/constants/user.constant';
import { FormBodySwagger } from 'src/constants/swagger.constants';
import { Response } from 'express';
import {
  AddQuestionToFieldDTO,
  CreateFieldWithQuestionsDTO,
  CreateQuestionFieldDTO,
} from './dtos/create-field-with-questions';
import { CookieNames, ReturnStatus } from 'src/auth/constants/constants';
import { ReturnNotification } from 'src/constants/entity.constants';
import { UpdateNameFieldDTO } from './dtos/update-field';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
@Controller('fields')
@ApiTags('fields')
@ApiBearerAuth('access-token')
@UseGuards(JwtAuthGuard)
export class FieldController {
  constructor(private fieldService: FieldService) {}

  // get all field
  @Get()
  @ApiOperation({ summary: 'Get all field' })
  async getAll() {
    return await this.fieldService.getAll();
  }

  // /* get field by id
  //  */
  @Get(':id')
  @ApiOperation({ summary: 'Get question by field Id or name' })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async getFieldWithQuestion(@Param('id') id: string): Promise<Field> {
    return await this.fieldService.getFieldById(id);
  }

  /*
  query field by id or name
  */
  @Get('get/:query')
  @ApiOperation({ summary: 'Get question by field Id or name' })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async queryFieldByQueryString(@Param('query') query: string): Promise<Field> {
    return await this.fieldService.queryField(query);
  }

  //   create field with questions
  @Post('create/with-questions')
  @Roles(RoleUser.Admin)
  @ApiOperation({ summary: 'Create new field with questions' })
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async createWithQues(
    @Body() createFieldQuest: CreateFieldWithQuestionsDTO,
  ): Promise<Field> {
    try {
      return await this.fieldService.createFieldQuest(createFieldQuest);
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.CREATED);
    }
  }

  // //   create just new field
  @Post('create')
  @Roles(RoleUser.Admin)
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @ApiOperation({ summary: 'Create new Field By Admin' })
  @ApiBody({ type: CreateFieldDTO })
  async createField(@Body() createField: CreateFieldDTO): Promise<Field> {
    console.log('Create Field');
    try {
      return await this.fieldService.createField(createField);
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.CREATED);
    }
  }

  /* get  questions by field id
  add questions to cookie and post to answer questions
  */
  @Get(':id/questions')
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @ApiOperation({ summary: 'Get field by id' })
  async getFieldQuestion(
    @Param('id') id: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<Question[]> {
    const questions = await this.fieldService.getQuestionFromField(id);
    res.cookie(CookieNames.FieldId, id);
    res.cookie(CookieNames.Questions, questions);
    return questions;
  }

  /* get  questions by field id
  field id get from when create resume
  */
  // @Get('aa/resume/questions')
  // @ApiOperation({ summary: 'Get question by field Id' })
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // async getResumeQuestion(
  //   @Cookies(CookieNames.FieldId) fieldId: string,
  //   @Res({ passthrough: true }) res: Response,
  // ): Promise<Question[]> {
  //   console.log(fieldId);
  //   console.log('**');

  //   const questions = await this.fieldService.getQuestionFromField(fieldId);
  //   res.cookie(CookieNames.Questions, questions);
  //   return questions;
  // }

  /* post answer to questions
  get questions from cookie and post answer to questions
  */
  // @Post('answer')
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // async answerQuestionFromField(
  //   @Body() answers: CreateAnswerQuestion[],
  //   @Cookies(CookieNames.JWT) cookie,
  // ) {
  //   return this.fieldService.answerQuestionsFromField(cookie, answers);
  // }

  //   add question to field
  @Post(':id')
  @Roles(RoleUser.Admin)
  @ApiOperation({ summary: 'Add question to field' })
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // @ApiConsumes(FormBodySwagger.FormJson)
  async addQuestionToField(
    @Param('id') id: string,
    @Body() questionDto: AddQuestionToFieldDTO,
  ): Promise<ReturnNotification> {
    try {
      console.log(questionDto);
      console.log(id);

      // const field = await this.fieldService.addQuestionToField(
      await this.fieldService.addQuestionToField(id, questionDto.questions);
      // return { message: ReturnStatus.Update, value: field };
      return {
        message: ReturnStatus.Update,
        value: questionDto.questions,
      };
      // return null;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_FOUND);
    }
  }

  // remove question in field
  // @Delete(':questionId')
  // @ApiOperation({
  //   summary: 'Delete question in field by field id and question id',
  // })
  // async removeQuestionInField(
  //   @Param('fieldId') fieldId: string,
  //   @Param('questionId') questionId: string,
  // ) {
  //   await this.removeQuestionInField(fieldId, questionId);
  //   return ReturnStatus.Delete;
  // }

  // update name field
  @Patch('update')
  @Roles(RoleUser.Admin)
  @ApiOperation({
    summary: 'Update name field by field Id',
  })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async updateNameField(
    @Body() updateFieldName: UpdateNameFieldDTO,
  ): Promise<ReturnNotification> {
    const field = await this.fieldService.updateName(
      updateFieldName.queryString,
      updateFieldName.nameField,
    );
    return {
      message: ReturnStatus.Update,
      value: field,
    };
  }

  // remove field
  // @Delete(':id')
  // @ApiOperation({
  //   summary: 'Remove field by field Id',
  // })
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // async removeField(@Param('id') id: string) {
  //   return await this.fieldService.removeField(id);
  // }
  // @Put('forceCreate')
  // async forceCreate(): Promise<Field[]> {
  //   const listField = [];
  //   const filed = FieldJob('IT', questIt);
  //   const filed2 = FieldJob('Chef', questChef);
  //   // console.log(filed);
  //   // console.log(filed2);
  //   listField.push(await this.fieldService.createFieldQuest(filed));
  //   listField.push(await this.fieldService.createFieldQuest(filed2));

  //   return listField;
  // }

  // //create question by topic
  // @Post('/add/addQuestion')
  // @ApiOperation({ summary: 'Add question to Field By Topic and Field Name' })
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // async createQuestion(@Body() formQuestionDto: NewQuestionDTO) {
  //   return await this.fieldService.addQuestByTopic(formQuestionDto);
  // }

  @Put('delete/:nameField')
  @ApiOperation({ summary: 'sort delete field' })
  async deleteField(@Param('nameField') nameField: string) {
    return this.fieldService.delete(nameField);
  }
}
