import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { use } from 'passport';
import { ReturnStatus } from 'src/auth/constants/constants';
import { TopicsArr, TopicsArrCommon } from 'src/constants/topic.constant';
import { Question } from 'src/questions/entities/questions.entity';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateTopicQuestionDTO } from './dtos/create-topicsQuestion.dto';
import { TopicQuestion } from './entities/topicQuestion.entity';

@Injectable()
export class TopicQuestionService {
  constructor(
    @InjectRepository(TopicQuestion)
    private topicQuestionRepository: Repository<TopicQuestion>,
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
  ) {}

  async addTopic(): Promise<string> {
    await this.add('Separate', TopicsArr);
    await this.add('Common', TopicsArrCommon);
    return ReturnStatus.Success;
  }

  async add(type, arrTopic) {
    for (let i = 0; i < arrTopic.length; i++) {
      const topic = this.topicQuestionRepository.create({
        topicQuestion: arrTopic[i],
        typeTopic: type,
      });
      await this.topicQuestionRepository.save(topic);
    }
  }

  async sortTopics(): Promise<TopicQuestion[]> {
    return this.topicQuestionRepository
      .createQueryBuilder('topic')
      .orderBy('topic.createAt', 'DESC')
      .getMany();
  }

  async getInforUserCurrent(user: User): Promise<User> {
    return await this.userRepository.findOne({ email: user.email });
  }

  async getQuesTopicCommon(topic): Promise<Question[]> {
    const topicFind = await this.topicQuestionRepository.findOne({
      topicQuestion: topic.topicQuestion,
    });

    const arrQuestions = await this.questionRepository.find({
      topicQuestion: topicFind,
    });
    return arrQuestions;
  }
}
