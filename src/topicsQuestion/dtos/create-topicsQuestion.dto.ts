import { ApiProperty } from '@nestjs/swagger';
import { Topics, TopicsCommon } from 'src/constants/topic.constant';

export class CreateTopicQuestionDTO {
  @ApiProperty({
    type: 'enum',
    enum: Topics,
    description: 'Topics of Question',
  })
  topicQuestion: Topics;
}

export class topicQuestionCommonDto {
  @ApiProperty({ type: 'enum', enum: TopicsCommon })
  topicQuestion: string;
}

export class CreateTopicDTO {
  @ApiProperty()
  topicQuestion: Topics;

  @ApiProperty()
  typeTopic: string;
}
