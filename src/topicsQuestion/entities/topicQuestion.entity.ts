// import { Field } from 'src/fileds/entities/field.entity';
import { BaseEntity } from 'src/model/base.entity';
import { Question } from 'src/questions/entities/questions.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TopicQuestion extends BaseEntity {
  @Column()
  topicQuestion: string;

  @Column({ nullable: true })
  typeTopic: string;

  @OneToMany(() => Question, (question) => question.topicQuestion)
  question: Question[];
}
