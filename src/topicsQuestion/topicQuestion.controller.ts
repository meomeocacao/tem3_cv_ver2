import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { FormBodySwagger } from 'src/constants/swagger.constants';
import { Roles } from 'src/decorators/roles.decorator';
import { Question } from 'src/questions/entities/questions.entity';
import { RoleUser } from 'src/users/constants/user.constant';
import {
  CreateTopicDTO,
  CreateTopicQuestionDTO,
  topicQuestionCommonDto,
} from './dtos/create-topicsQuestion.dto';
import { Response } from 'express';
import { TopicQuestionService } from './topicQuestion.service';
import { CookieNames } from 'src/auth/constants/constants';

@Controller('topic')
@ApiTags('topic')
@UseGuards(JwtAuthGuard, RolesGuard)
@ApiBearerAuth('access-token')
export class TopicQuestionController {
  constructor(private readonly topicQuestionService: TopicQuestionService) {}

  // @Roles(RoleUser.Admin)
  @Post('add-topic')
  async addTopic() {
    return await this.topicQuestionService.addTopic().then((result) => {
      return result;
    });
  }

  @Get('sort')
  async sortTopics() {
    return await this.topicQuestionService.sortTopics().then((result) => {
      return result;
    });
  }

  @ApiOperation({
    summary: 'Get questions of topic common',
  })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @Post('Get-Topic-common')
  async GetTopicQuestion(
    @Body() topic: topicQuestionCommonDto,
    @Res({ passthrough: true }) res: Response,
  ): Promise<Question[]> {
    const questions = await this.topicQuestionService.getQuesTopicCommon(topic);
    res.cookie(CookieNames.Questions, questions);
    return questions;
  }

  @Post('add-topic-raw')
  @ApiOperation({
    summary: 'add topic manipulation',
  })
  @ApiConsumes('multipart/form-data')
  async addTopicRaw(@Body() body: CreateTopicDTO): Promise<any> {
    console.log(body);
  }
}
