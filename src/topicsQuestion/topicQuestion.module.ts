import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Question } from 'src/questions/entities/questions.entity';
import { User } from 'src/users/entities/user.entity';
import { TopicQuestion } from './entities/topicQuestion.entity';
import { TopicQuestionController } from './topicQuestion.controller';
import { TopicQuestionService } from './topicQuestion.service';

@Module({
  imports: [TypeOrmModule.forFeature([TopicQuestion, User, Question])],
  controllers: [TopicQuestionController],
  providers: [TopicQuestionService],
  exports: [TopicQuestionService],
})
export class TopicQuestionModule {}
