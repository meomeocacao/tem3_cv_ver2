import {MigrationInterface, QueryRunner} from "typeorm";

export class newTable1634522765424 implements MigrationInterface {
    name = 'newTable1634522765424'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`topic_question\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`topicQuestion\` varchar(255) NOT NULL, \`typeTopic\` varchar(255) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`question\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`contentQuestion\` varchar(255) NOT NULL, \`titleQuestion\` varchar(255) NULL, \`topicQuestionId\` char(36) NULL, \`fieldId\` char(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`email\` varchar(255) NOT NULL, \`password\` varchar(255) NULL, \`fullName\` varchar(255) NOT NULL, \`role\` enum ('Admin', 'User') NOT NULL DEFAULT 'User', \`gender\` enum ('Male', 'Female', 'Default') NOT NULL DEFAULT 'Male', \`avatarUrl\` varchar(255) NOT NULL, \`dateOfBirth\` datetime NULL, \`provider\` varchar(255) NOT NULL DEFAULT 'Default', UNIQUE INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`invite\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`sender\` varchar(255) NOT NULL, \`receiverId\` char(36) NULL, UNIQUE INDEX \`REL_6f83b3b99409500392a534a1f7\` (\`receiverId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`field\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`nameField\` varchar(255) NOT NULL, \`fieldCode\` varchar(255) NOT NULL, UNIQUE INDEX \`IDX_d77222684fc86216812fb7c03d\` (\`nameField\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`resume\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`nameResume\` varchar(255) NOT NULL, \`resumeCode\` varchar(255) NOT NULL, \`resumeVersion\` varchar(255) NOT NULL, \`imageResume\` varchar(255) NOT NULL, \`version\` int NOT NULL, \`userId\` char(36) NULL, \`fieldId\` char(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`answer_question\` (\`id\` char(36) NOT NULL, \`createAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updateAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`isDelete\` tinyint NOT NULL DEFAULT 0, \`answerQuestion\` varchar(255) NULL, \`questionId\` char(36) NULL, \`resumeId\` char(36) NULL, \`userId\` char(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`question\` ADD CONSTRAINT \`FK_b6e3bdaa01c6281d37a3b76f0cb\` FOREIGN KEY (\`topicQuestionId\`) REFERENCES \`topic_question\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`question\` ADD CONSTRAINT \`FK_c48f9e17b293a2b7c07ee800d51\` FOREIGN KEY (\`fieldId\`) REFERENCES \`field\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`invite\` ADD CONSTRAINT \`FK_6f83b3b99409500392a534a1f76\` FOREIGN KEY (\`receiverId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`resume\` ADD CONSTRAINT \`FK_6543e24d4d8714017acd1a1b392\` FOREIGN KEY (\`userId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`resume\` ADD CONSTRAINT \`FK_7f5baa677b743c33aa74fdf4213\` FOREIGN KEY (\`fieldId\`) REFERENCES \`field\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`answer_question\` ADD CONSTRAINT \`FK_bb0debdb1de26b914da0b5f29b1\` FOREIGN KEY (\`questionId\`) REFERENCES \`question\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`answer_question\` ADD CONSTRAINT \`FK_a84fc841cb2d7819646aecdd1dd\` FOREIGN KEY (\`resumeId\`) REFERENCES \`resume\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`answer_question\` ADD CONSTRAINT \`FK_bc2c405f0b6a6ec83b405237b95\` FOREIGN KEY (\`userId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`answer_question\` DROP FOREIGN KEY \`FK_bc2c405f0b6a6ec83b405237b95\``);
        await queryRunner.query(`ALTER TABLE \`answer_question\` DROP FOREIGN KEY \`FK_a84fc841cb2d7819646aecdd1dd\``);
        await queryRunner.query(`ALTER TABLE \`answer_question\` DROP FOREIGN KEY \`FK_bb0debdb1de26b914da0b5f29b1\``);
        await queryRunner.query(`ALTER TABLE \`resume\` DROP FOREIGN KEY \`FK_7f5baa677b743c33aa74fdf4213\``);
        await queryRunner.query(`ALTER TABLE \`resume\` DROP FOREIGN KEY \`FK_6543e24d4d8714017acd1a1b392\``);
        await queryRunner.query(`ALTER TABLE \`invite\` DROP FOREIGN KEY \`FK_6f83b3b99409500392a534a1f76\``);
        await queryRunner.query(`ALTER TABLE \`question\` DROP FOREIGN KEY \`FK_c48f9e17b293a2b7c07ee800d51\``);
        await queryRunner.query(`ALTER TABLE \`question\` DROP FOREIGN KEY \`FK_b6e3bdaa01c6281d37a3b76f0cb\``);
        await queryRunner.query(`DROP TABLE \`answer_question\``);
        await queryRunner.query(`DROP TABLE \`resume\``);
        await queryRunner.query(`DROP INDEX \`IDX_d77222684fc86216812fb7c03d\` ON \`field\``);
        await queryRunner.query(`DROP TABLE \`field\``);
        await queryRunner.query(`DROP INDEX \`REL_6f83b3b99409500392a534a1f7\` ON \`invite\``);
        await queryRunner.query(`DROP TABLE \`invite\``);
        await queryRunner.query(`DROP INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` ON \`user\``);
        await queryRunner.query(`DROP TABLE \`user\``);
        await queryRunner.query(`DROP TABLE \`question\``);
        await queryRunner.query(`DROP TABLE \`topic_question\``);
    }

}
