import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { Field } from 'src/fileds/entities/field.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ReturnStatus } from 'src/auth/constants/constants';
import { FieldService } from 'src/fileds/fields.service';
import { Resume } from 'src/resumes/entities/resume.entity';
import { TopicQuestionService } from 'src/topicsQuestion/topicQuestion.service';
import { Repository } from 'typeorm';
import { CreateQuestionDTO } from './dtos/create-question.dto';
import { NewQuestionDTO, UpdateQuestionDTO } from './dtos/update-question.dto';
import { Question } from './entities/questions.entity';
import {
  questAcount,
  questChef,
  questionCommon,
  questIt,
} from 'src/constants/question.constant';
import { Topics, TopicsCommon, TypeTopic } from 'src/constants/topic.constant';
import { IsDeleteEntity } from 'src/constants/entity.constants';

export class QuestionService {
  constructor(
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
    @InjectRepository(TopicQuestion)
    private topicQuestionRepository: Repository<TopicQuestion>,
    @InjectRepository(Field)
    private fieldRepository: Repository<Field>,
  ) {}

  async addQuestion(): Promise<string> {
    try {
      await this.fieldRepository.save(
        this.fieldRepository.create({ nameField: 'IT' }),
      );
      await this.fieldRepository.save(
        this.fieldRepository.create({ nameField: 'Chef' }),
      );
      for (const item in Topics) {
        if (isNaN(Number(item))) {
          await this.topicQuestionRepository.save(
            this.topicQuestionRepository.create({
              topicQuestion: item,
              typeTopic: TypeTopic.Separate,
            }),
          );
        }
      }
      for (const item in TopicsCommon) {
        if (isNaN(Number(item))) {
          await this.topicQuestionRepository.save(
            this.topicQuestionRepository.create({
              topicQuestion: item,
              typeTopic: TypeTopic.Common,
            }),
          );
        }
      }
      await this.add(questionCommon);
      await this.add(questIt);
      await this.add(questChef);
      await this.add(questAcount);
      return ReturnStatus.Success;
    } catch {
      throw new Error('you added the question which exitting in your database');
    }
  }
  async add(QuestionArr): Promise<void> {
    for (let i = 0; i < QuestionArr.length; i++) {
      const question = this.questionRepository.create({
        contentQuestion: QuestionArr[i].question,
        topicQuestion: await this.topicQuestionRepository.findOne({
          topicQuestion: QuestionArr[i].topic,
        }),
        field: await this.fieldRepository.findOne({
          isDelete: IsDeleteEntity.False,
          nameField: QuestionArr[i]?.nameField,
        }),
        titleQuestion: QuestionArr[i].titleQuestion,
      });
      const check = await this.questionRepository.findOne({
        contentQuestion: QuestionArr[i].question,
      });
      if (!check) {
        await this.questionRepository.save(question);
      } else {
        continue;
      }
      await this.questionRepository.save(question);
    }
  }
}
