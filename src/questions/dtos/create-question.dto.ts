import { ApiProperty } from '@nestjs/swagger';
import { AnswerQuestionDTO } from 'src/answerQuestions/dtos/answer-question.dto';
import { Topics } from 'src/constants/topic.constant';
import { Question } from '../entities/questions.entity';

export class CreateQuestionDTO {
  @ApiProperty({ type: 'string', description: 'Question about the topic' })
  contentQuestion: string;
  @ApiProperty({ type: 'enum', enum: Topics })
  topicQuestion: Topics;
}

export class ReturnQuestionDTO {
  id: string;
  question: string;
  topic: string;
  answerId: string[];
  constructor(
    _id: string,
    _question: string,
    _topic: string,
    _answers: string[],
  ) {
    this.id = _id;
    this.question = _question;
    this.topic = _topic;
    this.answerId = _answers;
  }

  public set setAnswerId(answerIds: string[]) {
    for (let i = 0; i < answerIds.length; i++) {
      this.answerId.push(answerIds[i]);
    }
  }
  public get getAnswerId(): string[] {
    return this.answerId;
  }
}

// export const ReturnQuestion = (questions: Question): ReturnQuestionDTO => {
//   const answerIds: string[] = [];
//   for (let i = 0; i < questions.answerQuestion.length; i++) {
//     answerIds.push(questions.answerQuestion[i].id);
//   }
//   const question = new ReturnQuestionDTO(
//     questions.id,
//     questions.contentQuestion,
//     questions.topicQuestion.topicQuestion,
//     answerIds,
//   );
//   return question;
// };

// export const ReturnQuestions = (questions: Question[]): ReturnQuestionDTO[] => {
//   const returnArr = [];
//   for (let i = 0; i < questions.length; i++) {
//     returnArr.push(ReturnQuestion(questions[i]));
//   }
//   return returnArr;
// };
