import { ApiProperty } from '@nestjs/swagger';
import { Topics } from 'src/constants/topic.constant';
import { Field } from 'src/fileds/entities/field.entity';

export class UpdateQuestionDTO {
  @ApiProperty({ type: 'string', description: 'New content question' })
  contentQuestion: string;
}

export class NewQuestionDTO {
  @ApiProperty({ type: 'string', description: 'Field name' })
  nameField: string;
  @ApiProperty({ type: 'enum', enum: Topics })
  topicQuestion: Topics;
  @ApiProperty({ type: 'string', description: 'Question about the topic' })
  contentQuestion: string;
}
