import { Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ReturnStatus } from 'src/auth/constants/constants';
import { QuestionService } from './questions.service';

@Controller('questions')
@ApiTags('questions')
export class QuestionController {
  constructor(private readonly questionsService: QuestionService) {}

  @Post('add') // funstion hard code for everyone use
  async addQuestion() {
    return this.questionsService.addQuestion().then((result) => {
      return result;
    });
  }
}
