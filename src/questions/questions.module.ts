import { Controller, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Field } from 'src/fileds/entities/field.entity';
import { FieldsModule } from 'src/fileds/fields.module';
import { FieldService } from 'src/fileds/fields.service';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { TopicQuestionModule } from 'src/topicsQuestion/topicQuestion.module';
import { Question } from './entities/questions.entity';
import { QuestionController } from './question.controller';
import { QuestionService } from './questions.service';

@Module({
  imports: [TypeOrmModule.forFeature([Question, TopicQuestion, Field])],
  controllers: [QuestionController],
  providers: [QuestionService],
  exports: [QuestionService],
})
export class QuestionModule {}
