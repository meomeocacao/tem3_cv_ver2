import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Field } from 'src/fileds/entities/field.entity';

import { CreateQuestionDTO } from '../dtos/create-question.dto';
import { BaseEntity } from 'src/model/base.entity';

@Entity()
export class Question extends BaseEntity {
  @Column()
  contentQuestion: string;

  @Column({ nullable: true })
  titleQuestion: string;

  @ManyToOne(() => TopicQuestion, (topicQuestion) => topicQuestion.question)
  topicQuestion: TopicQuestion;

  @OneToMany(() => AnswerQuestion, (answerQuestion) => answerQuestion.question)
  answerQuestion: AnswerQuestion[];

  @ManyToOne(() => Field, (field) => field.questions)
  field: Field;
}

export function getQuestIt(questIt: any[]) {
  const arr = [];
  for (let i = 0; i < questIt.length; i++) {
    const quest = new CreateQuestionDTO();
    quest.contentQuestion = questIt[i].question;
    quest.topicQuestion = questIt[i].topic;
    arr.push(quest);
  }
  return arr;
}
