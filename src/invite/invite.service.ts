import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsDeleteEntity } from 'src/constants/entity.constants';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { Invite } from './entities/invite.entity';
import { InviteRelation } from './inviteConstants/relation-invite.constant';

@Injectable()
export class InviteService {
  constructor(
    @InjectRepository(Invite)
    private readonly inviteRepository: Repository<Invite>,
  ) {}
  async create(idSender: string, _receiver: User): Promise<Invite> {
    const newInvite = this.inviteRepository.create({
      sender: idSender,
      receiver: _receiver,
    });
    return await this.inviteRepository.save(newInvite);
  }

  async findAllInvite(id: string): Promise<Invite[]> {
    return this.inviteRepository.find({
      where: { sender: id, isDelete: IsDeleteEntity.False },
      relations: InviteRelation,
    });
  }

  async findOneUser(_receiver: User) {
    return await this.inviteRepository.findOne({
      where: { receiver: _receiver, isDelete: IsDeleteEntity.False },
    });
  }

  // async delete(receiver: User) {
  //   const invite = await this.findOneUser(receiver);
  //   return await this.inviteRepository.update(invite.id, { isDelete: true });
  // }
}
