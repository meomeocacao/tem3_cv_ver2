import { BaseEntity } from 'src/model/base.entity';
import { User } from 'src/users/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

@Entity()
export class Invite extends BaseEntity {
  @OneToOne(() => User)
  @JoinColumn()
  receiver: User;

  @Column()
  sender: string;
}
