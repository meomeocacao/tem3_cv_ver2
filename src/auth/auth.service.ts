import { jwtConstants } from 'src/auth/constants/constants';
import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';
import { EmailsService } from 'src/auth/emails/emails.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { AuthLoginDTO, CurrentUserDTO, EnterEmailDTO } from './dtos/auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private emailsService: EmailsService,
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    try {
      const user = await this.usersService.findByEmail(email);
      if (user && (await compare(password, user.password))) {
        return { ...user, password: undefined };
      }
      return null;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_FOUND);
    }
  }
  async login(user: any) {
    // console.log('login', user.role);
    const payload = { email: user.email, id: user.id, role: user.role };
    return this.jwtService.sign(payload);
  }

  async registerAccount(createUserDto: CreateUserDto, file: any, id?: any) {
    const user = await this.usersService.findByEmail(createUserDto.email);
    if (user) throw new NotImplementedException('Email already exists');
    if (
      !this.usersService.comparePassword(
        createUserDto.password,
        createUserDto.rePassword,
      )
    )
      throw new NotImplementedException('Password not the same');

    const token = this.jwtService.sign(
      { createUserDto, file, id },
      {
        secret: jwtConstants.secret,
        expiresIn: '300s',
      },
    );
    await this.emailsService.sendConfirmMail(createUserDto.email, token);
    return `send link confirm to ${createUserDto.email}`;
  }

  async confirmCreateAccount(token: any) {
    try {
      const convertToken = this.jwtService.verify(token, {
        secret: 'mot secret key',
      });
      return this.usersService.create(
        convertToken.createUserDto,
        convertToken.file,
        convertToken.id,
      );
    } catch (err) {
      return err.message;
    }
  }

  // send email reset password
  async sendResetPassword(forgetPassDTO: EnterEmailDTO) {
    const user = await this.usersService.findByEmail(forgetPassDTO.email);
    if (!user) throw new NotFoundException('Email is not registered');
    const token = this.jwtService.sign(
      { id: user.id },
      {
        secret: jwtConstants.secret,
        expiresIn: '1800s',
      },
    );
    await this.emailsService.sendForgetPassword(forgetPassDTO.email, token);
    return `Link to reset password sent to ${forgetPassDTO.email}`;
  }
}
