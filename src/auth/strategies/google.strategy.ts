import { ConflictException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { Provider } from 'src/users/constants/user.constant';
import { GoogleUserDto } from 'src/users/dto/google-user.dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from '../auth.service';
import { ReturnStatus } from '../constants/constants';
import { GoogleConstants } from '../constants/provider-constants';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(
    private usersService: UsersService,
    private authService: AuthService,
  ) {
    super({
      clientID: GoogleConstants.googleClientId,
      clientSecret: GoogleConstants.googleSecret,
      callbackURL: GoogleConstants.callBackUrl,
      scope: ['email', 'profile'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    try {
      const { name, emails, photos } = profile;
      const user = {
        email: emails[0].value,
        fullName: name.givenName + ' ' + name.familyName,
        avatarUrl: photos[0].value,
        provider: Provider.Google,
      };
      const _user = await this.usersService.findByEmail(user.email);
      if (_user && !_user.password) {
        const token = await this.authService.login(_user);
        done(null, {
          _user,
          accessToken,
          message: ReturnStatus.Existed,
          token,
        });
      } else {
        const _user = await this.usersService.createUserGoogle(user);
        const token = await this.authService.login(_user);
        done(null, {
          _user,
          accessToken,
          message: ReturnStatus.Create,
          token,
        });
      }
    } catch (error) {
      throw new ConflictException(error.message);
    }
  }
}
