import { DefaultAvaUrl } from './../../users/constants/user.constant';
import { FacebookConstants } from '../constants/provider-constants';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from 'passport-facebook';
import { UsersService } from 'src/users/users.service';
import { Provider } from 'src/users/constants/user.constant';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { AuthService } from '../auth.service';
import { ReturnStatus } from '../constants/constants';

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    private authService: AuthService,
  ) {
    super({
      clientID: FacebookConstants.facebook_id,
      clientSecret: FacebookConstants.facebook_secret,
      callbackURL: FacebookConstants.callBackUrl,
      // scope: 'email',
      // profile: ['emails', 'name'],
    });
  }
  async validate(
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done: (err: any, user: any, info?: any) => void,
  ): Promise<any> {
    const payload = {
      fullName: profile.displayName,
      email: profile.id,
      provider: Provider.Facebook,
      avatarUrl: DefaultAvaUrl.Default,
    };
    const user = await this.userRepo.findOne({ email: payload.email });
    if (user && user.provider === Provider.Facebook) {
      const token = await this.authService.login(user);
      done(null, { user, message: ReturnStatus.Existed, token });
    } else {
      const newUser = this.userRepo.create(payload);
      const _user = await this.userRepo.save(newUser);
      const token = await this.authService.login(_user);
      done(null, { _user, message: ReturnStatus.Create, token });
    }
  }
}
