export const jwtConstants = {
  secret: 'mot secret key',
  expTime: '24h',
  userSecret: 'mot user secret key',
  userTime: '1200s',
  resetSecret: 'mot reset secret key',
};

/*
0 : ResumeId
1 : questions
2 : fieldId
3 : jwt
*/

export const CookieNames = {
  ResumeId: 'ResumeId',
  Questions: 'questions',
  FieldId: 'fieldId',
  JWT: 'jwt',
  AnswerId: 'answerId',
  LoginToken: 'loginToken',
  UserInformation: 'userInformation',
  InformationResume: 'informationResume',
  NameResume: 'nameResume',
  ListNameResumes: 'listNameResumes',
  InforResume: 'inforResume',
  ListQuestion: 'listQuestion',
};

export function clearAllCookies(res: any) {
  for (const [key, value] of Object.entries(CookieNames)) {
    res.clearCookie(value);
  }
}

export enum ReturnStatus {
  Create = 'Created',
  Update = 'Updated',
  Delete = 'Deleted',
  Success = 'Success',
  Restore = 'Restored',
  Existed = 'Existed',
}

export const HostEmail = {
  email: 'kovt831@gmail.com',
  password: 'kovt080399',
};

// export const RelationField = ['questions', 'questions.topicQuestion'];

// export const RelationResume = ['answerQuestions'];

export const MailerConfig = {
  transport: {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: HostEmail.email,
      pass: HostEmail.password,
    },
  },
  defaults: {
    from: '"CVMaker-Team3" <modules@nestjs.com>',
  },
};
