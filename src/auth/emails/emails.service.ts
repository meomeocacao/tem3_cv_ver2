import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { JwtService } from '@nestjs/jwt';
import { GoogleConstants } from '../constants/provider-constants';

@Injectable()
export class EmailsService {
  constructor(private readonly mailerService: MailerService) {}
  async sendConfirmMail(email: string, token: string): Promise<any> {
    await this.mailerService.sendMail({
      to: email,
      subject: 'Account activation link ✔',
      html: `<h2>Thank you for joining our service</h2>
      <span>Click <a href="${GoogleConstants.hostUrl}confirm/${token}">link</a> to activate your account</span>`,
    });
  }

  //send email authentication forget password
  async sendForgetPassword(email: string, token: string) {
    await this.mailerService.sendMail({
      to: email,
      subject: 'Forget password ✔',
      text: 'CV Maker',
      html: `<h2>This token to reset password</h2>
      <span>${token}</span>`,
    });
  }

  //send email invite
  async sendEmailInvite(email: string, id: any) {
    await this.mailerService.sendMail({
      to: email,
      subject: 'Invitation ✔',
      html: `<h2>Your friend has sent you an invitation to join our service</h2>
      <h3>Invite code: </h3><p>${id}</p>
      <span>You can also click on the <a href="${GoogleConstants.hostUrl}register/${id}">link</a> to register</span>`,
    });
  }
}
