import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { ROLES_KEY } from 'src/decorators/roles.decorator';
import { RoleUser } from 'src/users/constants/user.constant';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<RoleUser[]>(
      ROLES_KEY,
      [context.getHandler(), context.getClass()],
    );
    if (!requiredRoles) {
      return true;
    }
    // console.log('at here 1');
    const { user } = context.switchToHttp().getRequest();
    return requiredRoles?.some((role) => user.role?.includes(role));
  }
}

// @Injectable()
// export class RolesGuard implements CanActivate {
//   constructor(
//     private reflector: Reflector,
//     private usersService: UsersService,
//   ) {}

//   async canActivate(context: ExecutionContext) {
//     const roles = this.reflector.getAllAndOverride<RoleUser[]>(ROLES_KEY, [
//       context.getHandler(),
//       context.getClass(),
//     ]);
//     if (!roles) {
//       return true;
//     }
//     const request = context.switchToHttp().getRequest();
//     const user = request.user;
//     const userCurrent = await this.usersService.findOne(user.userId);
//     const result = roles.some((role) => userCurrent.role?.includes(role));
//     return result;
//   }
// }
