import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { UsersModule } from 'src/users/users.module';
import { GoogleStrategy } from '../strategies/google.strategy';
import { GoogleService } from './google.service';

@Module({
  imports: [UsersModule, AuthModule],
  providers: [GoogleService, GoogleStrategy],
  exports: [GoogleService],
})
export class GoogleModule {}
