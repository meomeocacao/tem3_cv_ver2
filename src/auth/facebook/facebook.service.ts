import { Injectable, NotFoundException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class FacebookService {
  constructor(private userServices: UsersService) {}

  loginFacebook(req: any) {
    if (!req.user) throw new NotFoundException('User not found');
    return {
      message: 'Profile of Facebook',
      user: req.user,
    };
  }
}
