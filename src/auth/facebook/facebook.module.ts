import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { AuthModule } from '../auth.module';
import { FacebookStrategy } from '../strategies/facebook.strategy';
import { FacebookService } from './facebook.service';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    forwardRef(() => AuthModule),
    UsersModule,
  ],
  providers: [FacebookStrategy, FacebookService],
  exports: [FacebookService],
})
export class FacebookModule {}
