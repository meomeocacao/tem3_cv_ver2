import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, Length } from 'class-validator';

export class AuthLoginDTO {
  @IsNotEmpty()
  @ApiProperty({
    description: 'username of account',
    default: '@example.com',
    format: 'email',
  })
  email: string;

  @IsNotEmpty()
  @ApiProperty({
    description: 'password of account',
    default: '123',
    format: 'password',
  })
  password: string;
}

export class EnterEmailDTO {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Email',
    default: '@example.com',
    format: 'email',
  })
  email: string;
}

export class CurrentUserDTO {
  userId: string;
  email: string;
}
