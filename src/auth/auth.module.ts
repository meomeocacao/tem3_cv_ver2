import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { EmailsModule } from 'src/auth/emails/emails.module';
import { EmailsService } from 'src/auth/emails/emails.service';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants/constants';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { FacebookService } from './facebook/facebook.service';
import { FacebookModule } from './facebook/facebook.module';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    EmailsModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: jwtConstants.expTime },
    }),
    FacebookModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, FacebookService],
  exports: [AuthService],
})
export class AuthModule {}
