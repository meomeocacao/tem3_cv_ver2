import { User } from 'src/users/entities/user.entity';
import { AuthService } from './auth/auth.service';
import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiHeader,
  ApiOperation,
  ApiProperty,
} from '@nestjs/swagger';
import {
  AuthLoginDTO,
  CurrentUserDTO,
  EnterEmailDTO,
} from './auth/dtos/auth.dto';
import { GoogleService } from './auth/google/google.service';
import { GGAuthGuard } from './auth/guards/google-auth.guard';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from './auth/guards/local-auth.guard';
import { CreateUserDto } from './users/dto/create-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { DestType, multerOptions } from './model/multer-option';
import { CurrentUser } from './decorators/currentUser.decorator';
import { UsersService } from './users/users.service';
import { CreateNewPassDto } from './users/dto/update-pass.dto';
import { TopicQuestionService } from './topicsQuestion/topicQuestion.service';
import { clearAllCookies, CookieNames } from './auth/constants/constants';
import { Roles } from './decorators/roles.decorator';
import { RolesGuard } from './auth/guards/roles.guard';
import { FacebookService } from './auth/facebook/facebook.service';
import { RoleUser } from './users/constants/user.constant';
import { FbAuthGuard } from './auth/guards/facebook-auth.guard';

@Controller()
export class AppController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private googleService: GoogleService,
    private topicService: TopicQuestionService,
    private facebookService: FacebookService,
  ) {}

  // login
  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiConsumes('application/x-www-form-urlencoded')
  async login(
    @Body() authLoginDto: AuthLoginDTO,
    @Res({ passthrough: true }) res,
  ) {
    try {
      if (!authLoginDto.password) throw new NotFoundException();
      const user = await this.authService.validateUser(
        authLoginDto.email,
        authLoginDto.password,
      );
      clearAllCookies(res);

      const token = await this.authService.login(user);
      res.cookie(CookieNames.LoginToken, token);
      return token;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_FOUND);
    }
  }

  // get profile
  @ApiBearerAuth('access-token')
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@CurrentUser() user: CurrentUserDTO) {
    return user;
  }

  //login win google
  @Get('google')
  @ApiOperation({ summary: 'Login with google' })
  @UseGuards(GGAuthGuard)
  async googleAuth(@Req() req) {
    console.log('Go to login with google');
  }

  //token by login google
  @Get('google/redirect')
  @UseGuards(GGAuthGuard)
  googleAuthRedirect(@Req() req) {
    return this.googleService.googleLogin(req);
  }

  // //login facebook
  // @Get('facebook')
  // @ApiOperation({ summary: 'Login with facebook' })
  // @UseGuards(FbAuthGuard)
  // async facebookLogin(): Promise<any> {
  //   return 'login facebook';
  // }

  //return token Facebook
  @Get('facebook')
  @ApiOperation({ summary: 'Login with facebook' })
  @UseGuards(FbAuthGuard)
  facebookAuthRedirect(@Req() req) {
    return this.facebookService.loginFacebook(req);
  }

  //register
  @Post('register')
  @ApiOperation({ summary: 'Register account' })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.User)))
  async register(@Body() createUserDto: CreateUserDto, @UploadedFile() file) {
    return await this.authService.registerAccount(createUserDto, file);
  }

  //register invite
  @Post('register/:codeInvite')
  @ApiOperation({ summary: 'Register account with invited code' })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.User)))
  async registerInvite(
    @Body() createUserDto: CreateUserDto,
    @Param('codeInvite') codeInvite: string,
    @UploadedFile() file,
  ) {
    return await this.authService.registerAccount(
      createUserDto,
      file,
      codeInvite,
    );
  }

  @Get('confirm/:token')
  @ApiOperation({ summary: 'Confirm register account with invite code' })
  async confirm(@Param('token') token: string) {
    return await this.authService.confirmCreateAccount(token);
  }

  @Post('forgetpassword')
  @ApiConsumes('application/x-www-form-urlencoded')
  async forgetPassword(@Body() forgetPassDto: EnterEmailDTO) {
    return this.authService.sendResetPassword(forgetPassDto);
  }

  @Post('resetpassword/:token')
  @ApiOperation({ summary: 'Confirm reset password with token sent' })
  @ApiConsumes('application/x-www-form-urlencoded')
  async resetPassword(
    @Body() createNewPassDto: CreateNewPassDto,
    @Param('token') token: string,
  ) {
    return await this.usersService.resetPassword(createNewPassDto, token);
  }

  // @Get('return/topic')
  // async returnTopic(): Promise<TopicQuestion[]> {
  //   // return await this.topicService.returnTopic();
  // }

  @Roles(RoleUser.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('total/user')
  @ApiBearerAuth('access-token')
  async getTotalUser() {
    return this.usersService.findAll();
  }
}
