import { HttpException, HttpStatus } from '@nestjs/common';

export class ValidatePassword extends HttpException {
  constructor() {
    super('Password are not same', HttpStatus.FORBIDDEN);
  }
}
