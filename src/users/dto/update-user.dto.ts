import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Gender } from 'src/users/constants/user.constant';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto {
  @ApiProperty({
    description: 'Fullname of user',
    default: 'John Wick',
    required: false,
  })
  fullName: string;

  @ApiProperty({
    type: 'string',
    format: 'date-time',
    description: 'Date of birth: yyyy-M-dd',
    required: false,
  })
  dateOfBirth: string;

  @ApiProperty({ enum: Gender })
  gender: Gender;
  @ApiProperty({
    type: 'file',
    description: 'Avatar Upload',
    required: false,
  })
  photo?: any;
  avatarUrl?: string;
}
