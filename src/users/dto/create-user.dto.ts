import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, Length } from 'class-validator';
import { Gender } from 'src/users/constants/user.constant';
import { Column } from 'typeorm';

export class CreateUserDto {
  @IsEmail()
  @ApiProperty({
    description: 'Email of account',
    default: '@example.com',
    format: 'email',
  })
  email: string;

  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  password: string;

  @Length(8, 20)
  @ApiProperty({
    description: 'type Password again',
    default: 'password',
    format: 'password',
  })
  rePassword: string;

  @ApiProperty({ description: 'Full name of user', default: 'John Wick' })
  fullName: string;

  @ApiProperty({ enum: Gender })
  gender: Gender;

  @ApiProperty({
    type: 'string',
    format: 'date-time',
    description: 'Date of birth: yyyy-M-dd',
    default: '2021-10-18',
    required: false,
  })
  dateOfBirth: string;

  @ApiProperty({
    type: 'file',
    description: 'Avatar Upload',
    required: false,
  })
  photo?: any;

  avatarUrl?: string;
}
