import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, Length } from 'class-validator';
import { Column } from 'typeorm';

export class GoogleUserDto {
  @IsEmail()
  email: string;
  fullName: string;
  avatarUrl: string;
}
