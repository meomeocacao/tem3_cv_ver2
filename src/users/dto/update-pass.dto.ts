import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';

export class UpdatePassDTO {
  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  oldPassword: string;
  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  newPassword: string;
  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  reNewPassword: string;
}

export class CreateNewPassDto {
  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  newPassword: string;

  @Length(8, 20)
  @ApiProperty({
    description: 'Password',
    default: 'password',
    format: 'password',
  })
  reNewPassword: string;
}
