export enum RoleUser {
  Admin = 'Admin',
  User = 'User',
}

export enum Gender {
  Male = 'Male',
  Female = 'Female',
  Default = 'Default',
}

export const DefaultAvaUrl = {
  Male: 'src/img/avatarDefault/male.jpg',
  Female: 'src/img/avatarDefault/female.jpg',
  Default: 'src/img/avatarDefault/default.png',
};

export const Provider = {
  Facebook: 'Facebook',
  Google: 'Google',
  Default: 'Default',
};

export const ReturnDefaultAvaUrl = (gender: Gender) => {
  return DefaultAvaUrl[gender];
};
