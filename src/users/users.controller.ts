import { FormBodySwagger } from 'src/constants/swagger.constants';
import { QuestionService } from 'src/questions/questions.service';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseInterceptors,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { DestType, multerOptions } from 'src/model/multer-option';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CurrentUser } from 'src/decorators/currentUser.decorator';
import { UpdatePassDTO } from './dto/update-pass.dto';
import { RolesGuard } from 'src/auth/guards/roles.guard';
// import { TopicQuestionCommonService } from 'src/topicQuestionCommon/topicQuestionCommon.service';
import { CurrentUserDTO, EnterEmailDTO } from 'src/auth/dtos/auth.dto';
import fs from 'fs/promises';
import { Roles } from '../decorators/roles.decorator';
import { CreateQuestionDTO } from 'src/questions/dtos/create-question.dto';
import { AuthGuard } from '@nestjs/passport';
// import { NewQuestionDTO } from 'src/questions/dtos/update-question.dto';
@Controller('users')
@ApiTags('users')
// @ApiBearerAuth('access-token')
// @UseGuards(JwtAuthGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // Create account
  @Post('create')
  @ApiConsumes(FormBodySwagger.FormData)
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.User)))
  async create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() file,
  ): Promise<User> {
    return await this.usersService.create(createUserDto, file);
  }

  // get user
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  // get user by id or email
  @Get('find/:emailOrId')
  findOne(@Param('emailOrId') emailOrId: string): Promise<User> {
    return this.usersService.findOne(emailOrId);
  }

  // update current user full name and avatar
  @Patch('update')
  @ApiConsumes(FormBodySwagger.FormData)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.User)))
  update(
    @CurrentUser() user: CurrentUserDTO,
    @UploadedFile() file,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateUser(user.email, updateUserDto, file);
  }

  // update password current user
  @Patch('password')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async updatePassword(
    @CurrentUser() user: any,
    @Body() updatePassDto: UpdatePassDTO,
  ) {
    return await this.usersService.updatePassword(user.email, updatePassDto);
  }

  // @ApiTags('answer-question')
  // @Roles('Admin')
  // @UseGuards(JwtAuthGuard, RolesGuard)
  // @Get('total/topicQesCom')
  // @ApiBearerAuth('access-token')
  // async getTotalUser(@CurrentUser() user) {
  //   console.log(user);
  //   return this.topicQuestionCommonService.checkTopicQuestionCommon();
  //   // return user;
  // }

  @Post('invite')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async emailInvite(
    @CurrentUser() user: CurrentUserDTO,
    @Body() inviteEmail: EnterEmailDTO,
  ) {
    return this.usersService.emailInvite(inviteEmail, user);
  }

  // test delete img
  @Get('deleteImg/adsad')
  async deleteImg() {
    const path = './src/img/avaFemale.png';
    try {
      await fs.unlink(path);
      console.log('successfully deleted /tmp/hello');
    } catch (error) {
      console.error('there was an error:', error.message);
    }
  }

  @Get('invited-users')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  async getAllInvite(@CurrentUser() user: CurrentUserDTO) {
    return await this.usersService.getAllInvite(user.userId);
  }

  @Get('sender')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('access-token')
  async getUserByInvite(@CurrentUser() user: CurrentUserDTO) {
    return await this.usersService.getUserByInvite(user.userId);
  }

  // @Get('delete')
  // @UseGuards(JwtAuthGuard)
  // @ApiBearerAuth('access-token')
  // async delete(@CurrentUser() user: CurrentUserDTO) {
  //   return await this.usersService.deleteUser(user.userId);
  // }
}
