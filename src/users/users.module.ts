import { QuestionModule } from 'src/questions/questions.module';
import { JwtStrategy } from './../auth/strategies/jwt.strategy';
import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from './users.repository';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants/constants';
import { EmailsService } from 'src/auth/emails/emails.service';
import { InviteModule } from 'src/invite/invite.module';
import { Invite } from 'src/invite/entities/invite.entity';

@Module({
  imports: [
    InviteModule,
    TypeOrmModule.forFeature([UsersRepository, Invite]),
    JwtModule.register({
      secret: jwtConstants.userSecret,
      signOptions: { expiresIn: jwtConstants.userTime },
    }),
  ],
  controllers: [UsersController],
  providers: [UsersService, JwtStrategy, EmailsService],
  exports: [UsersService],
})
export class UsersModule {}
