/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { GoogleConstants } from 'src/auth/constants/provider-constants';
import { CurrentUserDTO, EnterEmailDTO } from 'src/auth/dtos/auth.dto';
import { EmailsService } from 'src/auth/emails/emails.service';
import { ValidatePassword } from 'src/customException/custom-exceptions';
import { InviteService } from 'src/invite/invite.service';
import { Compare, Hashed } from 'src/model/hash-value';
import { DestType, removeImg } from 'src/model/multer-option';
import { Provider, RoleUser } from './constants/user.constant';
import { CreateUserDto } from './dto/create-user.dto';
import { GoogleUserDto } from './dto/google-user.dto';
import { UpdatePassDTO, CreateNewPassDto } from './dto/update-pass.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersRepository } from './users.repository';

@Injectable()
export class UsersService {
  constructor(
    private userRepository: UsersRepository,
    private jwtService: JwtService,
    private emailsService: EmailsService,
    private inviteService: InviteService,
  ) {}

  // create user
  async create(
    createUserDto: CreateUserDto,
    file: any,
    idInvite?: string,
  ): Promise<User> {
    if (!this.comparePassword(createUserDto.password, createUserDto.rePassword))
      throw new ValidatePassword();
    if (file) createUserDto.avatarUrl = this.returnImgUrl(file);
    try {
      const user = this.userRepository.create({
        ...createUserDto,
        role: RoleUser.Admin,
      });
      const _user = await this.userRepository.save(user);
      if (idInvite) this.inviteService.create(idInvite, _user);
      return _user;
    } catch (error) {
      return error.message;
    }
  }

  async createUserGoogle(userGoogle: GoogleUserDto): Promise<User> {
    const userGG = this.userRepository.create({
      ...userGoogle,
      provider: Provider.Google,
    });
    return await this.userRepository.save(userGG);
  }
  // find all users
  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }
  // get user by id
  async getUserById(id: string): Promise<User> {
    return await this.userRepository.findOne({ id });
  }
  // find one user by id
  async findOne(queryString: string): Promise<User> {
    const user = await this.userRepository.getUserByQueryString(queryString);
    return user;
  }
  // update user full name , avatar
  async updateUser(email: string, updateUserDto: UpdateUserDto, file: any) {
    // get user by id/ email
    const user = await this.findOne(email);
    // return full name
    if (!updateUserDto.fullName) updateUserDto.fullName = user.fullName;
    // return image url
    const urlAva = user.avatarUrl;
    updateUserDto.avatarUrl = file ? this.returnImgUrl(file) : user.avatarUrl;
    // remove old img
    removeImg(urlAva.toString());
    const { photo, ...rest } = updateUserDto;
    await this.userRepository.update(user.id, {
      ...rest,
    });
    return await this.findOne(email);
    // return 'Updated';
  }

  // update password
  async updatePassword(email: string, updatePassDto: UpdatePassDTO) {
    // get current user
    const user = await this.userRepository.getUserByQueryString(email);
    // check current password and old password input

    const isMatch = await Compare(updatePassDto.oldPassword, user.password);
    if (!isMatch) throw new ValidatePassword();
    // compare password and re type
    const confirm = this.comparePassword(
      updatePassDto.newPassword,
      updatePassDto.reNewPassword,
    );
    if (!confirm) throw new ValidatePassword();
    /**
     *
     ***confirm token
     *
     * */
    const hashed = await Hashed(updatePassDto.newPassword);
    await this.userRepository.update(user.id, {
      password: hashed,
    });
    return 'Updated';
  }

  findByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email });
  }

  // return img url
  returnImgUrl(file: any): string {
    return GoogleConstants.hostUrl + DestType.User + file.filename;
  }

  // compare two password
  comparePassword(password: string, rePassword: string) {
    if (password == rePassword) return true;
    return false;
  }

  // reset password
  async resetPassword(createNewPassDto: CreateNewPassDto, token: string) {
    try {
      const convertToken = await this.jwtService.verify(token, {
        secret: 'mot secret key',
      });
      const check = this.comparePassword(
        createNewPassDto.newPassword,
        createNewPassDto.reNewPassword,
      );
      if (check) {
        await this.userRepository.update(convertToken.id, {
          password: await Hashed(createNewPassDto.newPassword),
        });
        return 'Password reset successful';
      }
      return 'Password are not same';
    } catch (err) {
      return err.message;
    }
  }

  //send email invite
  async emailInvite(inviteEmail: EnterEmailDTO, user: CurrentUserDTO) {
    const checkUser = await this.findByEmail(inviteEmail.email);
    if (checkUser) throw new NotFoundException('The Email was registered');
    await this.emailsService.sendEmailInvite(inviteEmail.email, user.userId);
    return `Sent an invitation to ${inviteEmail.email}`;
  }

  // async deleteUser(id: string) {
  //   //  await this.userRepository.findOne(id);
  //   const updateUser = await this.userRepository.update(id, { isDelete: true });
  //   const user = await this.findOne(id);
  //   await this.inviteService.delete(user);
  // }

  async getAllInvite(id: string) {
    return await this.inviteService.findAllInvite(id);
  }

  async getUserByInvite(id: string) {
    const receiver = await this.userRepository.findOne(id);
    const sender = await this.inviteService.findOneUser(receiver);
    if (!sender) throw new NotFoundException('Not found');
    return await this.userRepository.findOne(sender.sender);
  }
}
