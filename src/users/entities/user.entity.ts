import { IsEmail } from 'class-validator';
import { BaseEntity } from 'src/model/base.entity';
import { BeforeInsert, Column, Entity, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { v4 as uuid } from 'uuid';
import { Resume } from 'src/resumes/entities/resume.entity';
import { Hashed } from 'src/model/hash-value';
import {
  Gender,
  Provider,
  ReturnDefaultAvaUrl,
  RoleUser,
} from 'src/users/constants/user.constant';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
// import { Invite } from 'src/invite/entities/invite.entity';

@Entity()
export class User extends BaseEntity {
  @Column({ unique: true })
  @IsEmail()
  email: string;

  @Column({ nullable: true })
  @Exclude()
  password!: string;

  @Column()
  fullName: string;

  @Column({ type: 'enum', enum: RoleUser, default: RoleUser.User })
  role: RoleUser;

  @Column({ type: 'enum', enum: Gender, default: Gender.Male })
  gender: Gender;

  @Column()
  avatarUrl: string;

  @Column({ nullable: true })
  dateOfBirth: Date;

  @Column({ default: Provider.Default })
  provider: string;

  @OneToMany(() => Resume, (resume) => resume.user)
  resume: Resume[];

  // @OneToOne(() => Invite, (invites) => invites.sender)
  // invite: Invite;
  @OneToMany(() => AnswerQuestion, (answerQuestion) => answerQuestion.user)
  answerQuestion: AnswerQuestion[];

  @BeforeInsert()
  async hashPassAndGenInvite() {
    this.id = uuid();
    if (this.password) this.password = await Hashed(this.password);
    // this.codeInvite = InviteUrl(this.id);
    if (!this.avatarUrl) this.avatarUrl = ReturnDefaultAvaUrl(this.gender);
  }
}
