import { EntityRepository, Repository } from 'typeorm';
import { User } from './entities/user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  // get user by query String
  async getUserByQueryString(queryString: string): Promise<User | undefined> {
    return await this.createQueryBuilder('user')
      .where('user.id = :queryString', { queryString })
      .orWhere('user.email = :queryString', { queryString })
      .getOne();
  }

  //   get password from user
  async getPassword(id: string) {
    const user = await this.findOne({ id });
    return user.password;
  }
}
