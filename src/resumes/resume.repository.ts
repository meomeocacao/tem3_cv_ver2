import { ResumeDTO } from './dtos/resume.dto';
import { Brackets, EntityRepository, Repository } from 'typeorm';
import { Resume } from './entities/resume.entity';
import { NotImplementedException, NotFoundException } from '@nestjs/common';
import { Field } from 'src/fileds/entities/field.entity';
import { IsDeleteEntity } from 'src/constants/entity.constants';
import { ReturnResumeRelations } from './constants/resume.constant';

@EntityRepository(Resume)
export class ResumeRepository extends Repository<Resume> {
  //get all Resume
  async getResumeUser(id: string) {
    const check = await this.createQueryBuilder('ResumeEntity')
      .innerJoinAndSelect('ResumeEntity.field', 'piledId')
      .innerJoinAndSelect('ResumeEntity.answerQuestions', 'answerQuestion')
      .leftJoinAndSelect('answerQuestion.question', 'question')
      // .leftJoinAndSelect('question.field', 'field')
      .leftJoinAndSelect('question.topicQuestion', 'topicQuestion')
      .where('userId =:id', { id })
      .andWhere('isDelete = false')
      .getMany();
    return check;
  }

  // check Resume exist in user
  async getResumeInUser(ResumeId: string, userId: string): Promise<Resume> {
    const Resume = await this.createQueryBuilder('Resume')
      .leftJoinAndSelect('Resume.user', 'user')
      .leftJoinAndSelect('Resume.field', 'field')
      .leftJoinAndSelect('Resume.answerQuestions', 'answerQuestion')
      .leftJoinAndSelect('answerQuestion.question', 'question')
      // .leftJoinAndSelect('question.field', 'field')
      .leftJoinAndSelect('question.topicQuestion', 'topicQuestion')
      .where('user.id =:userId', { userId })
      .andWhere('Resume.id =:ResumeId', { ResumeId })
      .andWhere('Resume.isDelete = false')
      .getOne();
    return Resume;
  }
  // check Resume by id
  async getResumeById(ResumeId: string): Promise<Resume> {
    const Resume = await this.createQueryBuilder('Resume_entity')
      .leftJoinAndSelect('Resume_entity.field', 'piledId')
      .leftJoinAndSelect('Resume_entity.answerQuestions', 'answerQuestion')
      .leftJoinAndSelect('answerQuestion.question', 'question')
      // .leftJoinAndSelect('question.field', 'field')
      .leftJoinAndSelect('question.topicQuestion', 'topicQuestion')
      .where('Resume_entity.id =:ResumeId', { ResumeId })
      .andWhere('isDelete = false')
      .getOne();
    return Resume;
  }
  //create Resume
  async createResume(ResumeDto: ResumeDTO): Promise<any> {
    try {
      const check = await this.createQueryBuilder('Resume')
        .leftJoin('Resume.user', 'user')
        .leftJoinAndSelect('Resume.field', 'field')
        .andWhere('user.id = :userId', { userId: ResumeDto.user.id })
        .andWhere('field.id = :fieldId', { fieldId: ResumeDto.field.id })
        .getOne();

      if (!check || check.isDelete === true) {
        const newResume = this.create(ResumeDto);
        return await this.save(newResume);
      }
      throw new NotImplementedException('Resume already exist');
    } catch (err) {
      return err.message;
    }
  }

  async updateResume(
    userId: string,
    oldFieldId: string,
    newField: any,
  ): Promise<any> {
    const check = await this.createQueryBuilder('Resume')
      .leftJoin('Resume.user', 'user')
      .leftJoinAndSelect('Resume.field', 'field')
      .andWhere('user.id = :userId', { userId: userId })
      .andWhere('field.id = :fieldId', { fieldId: oldFieldId })
      .getOne();
    if (!check) throw new NotFoundException('Resume not found');
    check.field = newField;
    return await this.save(check);
  }

  async deleteResume(userId: string, fieldId: string): Promise<any> {
    const check = await this.createQueryBuilder('Resume')
      .leftJoin('Resume.user', 'user')
      .leftJoinAndSelect('Resume.field', 'field')
      .andWhere('user.id = :userId', { userId: userId })
      .andWhere('field.id = :fieldId', { fieldId: fieldId })
      .getOne();
    if (!check || check.isDelete === true)
      throw new NotFoundException('Resume not found');
    check.isDelete = true;
    await this.save(check);
    return 'delete successfully';
  }
  // get Resume by id by user id
  async getResumeByUser(userId: string, ResumeId: string): Promise<Resume> {
    return await this.createQueryBuilder('Resume')
      .leftJoinAndSelect('Resume.user', 'user')
      .where('user.id =:userId', { userId })
      .andWhere('Resume.id =:ResumeId', { ResumeId })
      .andWhere('Resume.isDelete = false')
      .getOne();
  }
  // get Resume is deleted
  // async getDeletedResume(ResumeId: string): Promise<Resume> {
  //   return await this.createQueryBuilder('Resume')
  //     .leftJoinAndSelect('Resume.field', 'field')
  //     .leftJoinAndSelect('Resume.answerQuestions', 'answerQuestions')
  //     .where('Resume.isDelete =true')
  //     .andWhere('Resume.id =:ResumeId', { ResumeId })
  //     .getOne();
  // }

  // get deleted resume
  async getDetetedResume(resumeId: string): Promise<Resume> {
    return await this.findOne({
      where: { id: resumeId, isDelete: IsDeleteEntity.True },
      relations: ReturnResumeRelations,
    });
  }

  // query resume
  async queryResume(resumeCode: string, fieldCode: string): Promise<Resume[]> {
    return await this.createQueryBuilder('resume')

      .leftJoinAndSelect('resume.field', 'field')
      .where('resume.isDelete =:delete', { delete: IsDeleteEntity.False })
      .andWhere(
        new Brackets((qb) => {
          qb.where('field.fieldCode =:fieldCode', { fieldCode }).orWhere(
            'resume.resumeCode=:resumeCode',
            { resumeCode },
          );
        }),
      )
      .getMany();
  }

  // query resume by user
  async queryResumeByUser(
    resumeCode: string,
    fieldCode: string,
    userId: string,
  ): Promise<Resume[]> {
    console.log(fieldCode);

    return await this.createQueryBuilder('resume')
      .leftJoinAndSelect('resume.field', 'field')
      .leftJoinAndSelect('resume.user', 'user')
      .where('resume.isDelete =:delete', { delete: IsDeleteEntity.False })
      .andWhere('user.id=:userId', { userId })
      .andWhere(
        new Brackets((qb) => {
          qb.where('field.fieldCode =:fieldCode', { fieldCode }).orWhere(
            'resume.resumeCode=:resumeCode',
            { resumeCode },
          );
        }),
      )
      .getMany();
  }
}
