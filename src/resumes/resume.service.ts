import { ConflictException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
import { Field } from 'src/fileds/entities/field.entity';
import { Question } from 'src/questions/entities/questions.entity';
import { IsNull, Repository } from 'typeorm';
import { Resume } from './entities/resume.entity';
import { ResumeRepository } from './resume.repository';
import * as fs from 'fs';
import PdfPrinter from 'pdfmake';
import { ReturnStatus } from 'src/auth/constants/constants';
import * as path from 'path';
import pdfMake from 'pdfmake/build/pdfmake';
import { User } from 'src/users/entities/user.entity';
import {
  EntityType,
  IsDeleteEntity,
  NameEntityCode,
  ReturnNotification,
} from 'src/constants/entity.constants';
import {
  ITHardQuestionList,
  ResumeDeleteRelation,
  ReturnResumeRelations,
} from './constants/resume.constant';
import { CreateResumeDTO } from './dtos/create-resume.dto';
import { DestType } from 'src/model/multer-option';
import { UpdateResumeDTO } from './dtos/update-resume.dto';
import { GetFieldRelation } from 'src/fileds/constants/field.constant';
import { AnswerQuestionDTO } from 'src/answerQuestions/dtos/answer-question.dto';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { AnswerResumeObjectDTO } from './dtos/answer-resume.dto';
export class ResumeService {
  constructor(
    @InjectRepository(ResumeRepository)
    private resumeRepository: ResumeRepository,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Field)
    private fieldRepository: Repository<Field>,
    @InjectRepository(AnswerQuestion)
    private answerRepository: Repository<AnswerQuestion>,
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
    @InjectRepository(TopicQuestion)
    private topicQuestion: Repository<TopicQuestion>,
  ) {}
  //   create Resume with user email
  //   async createResume(email: string, field: Fields): Promise<ResumeEntity> {
  //     const user = await this.usersService.findOne(email);
  //     const Resume = this.resumeRepository.create({
  //       field: field,
  //       user: user,
  //     });
  //   }

  // create resume
  async createResume(
    userId: string,
    newResume: CreateResumeDTO,
    filename: string,
  ): Promise<Resume> {
    newResume.imageResume = DestType.CV + filename;
    const fieldCode = NameEntityCode(newResume.fieldName, EntityType.Field);
    if (
      await this.resumeRepository.findOne({
        where: {
          resumeCode: NameEntityCode(newResume.nameResume, EntityType.Resume),
          user: { id: userId },
        },
      })
    )
      throw new ConflictException();
    const _field = await this.fieldRepository.findOne({
      where: { fieldCode, isDelete: IsDeleteEntity.False },
      relations: GetFieldRelation,
    });
    // if (!_field || !_field.questions) throw new NotFoundException();
    // const _user = await this.userRepository.findOne({
    //   where: { id: userId },
    // });
    const _resume = this.resumeRepository.create({
      ...newResume,
      user: { id: userId },
      field: _field,
    });

    const resume = await this.resumeRepository.save(_resume);
    if (!resume.field || !resume.field.questions) throw new NotFoundException();
    return resume;
  }

  /* answer question and create resume 
  if deleteAnswer = true => answer the question havent answer 
  */
  async answerAndSubmitResume(
    resumeId: string,
    // fieldId: string,
    questions: Question[],
    answers: AnswerResumeObjectDTO[],
    deleteAnswer?: boolean,
  ): Promise<Resume> {
    questions.forEach(async (question, index) => {
      answers[index].answerQuestion.forEach(async (answer) => {
        if (deleteAnswer)
          await this.answerRepository.delete({
            resume: { id: resumeId },
            question,
          });
        await this.answerRepository.save(
          this.answerRepository.create({
            answerQuestion: answer,
            question: question,
            resume: { id: resumeId },
          }),
        );
      });
    });

    return await this.resumeRepository.findOne({
      where: { id: resumeId },
      relations: ReturnResumeRelations,
    });
  }

  // update resume in current user
  async updateResume(
    resumeId: string,
    // userId: string,
    newResume: UpdateResumeDTO,
    file?: Express.Multer.File,
  ): Promise<Resume> {
    console.log(resumeId);
    const resume = await this.getResumeById(resumeId);
    // newResume.imageResume = filename
    //   ? DestType.CV + filename
    //   : resume.imageResume;
    // const { photo, ...rest } = newResume;
    const resumeCode = NameEntityCode(newResume.nameResume, EntityType.Resume);
    newResume.imageResume = file
      ? DestType.CV + file.filename
      : resume.imageResume;
    await this.resumeRepository.update(resumeId, {
      imageResume: newResume.imageResume,
      nameResume: newResume.nameResume,
      resumeCode,
      resumeVersion: newResume.resumeVersion,
    });

    return await this.resumeRepository.findOne({ id: resumeId });
  }

  // get resumes in current user
  async getResumeCurrentUser(userId: string): Promise<Resume[]> {
    const user = await this.userRepository.findOne({ id: userId });
    return await this.resumeRepository.find({
      where: { user, isDelete: IsDeleteEntity.False },
      relations: ReturnResumeRelations,
    });
  }

  // query resume by field or name in user or all
  async queryResume(
    fieldName?: string,
    resumeName?: string,
    userId?: string,
  ): Promise<Resume[]> {
    let fieldCode;
    if (fieldName) fieldCode = NameEntityCode(fieldName, EntityType.Field);

    let resumeCode;
    if (resumeName) resumeCode = NameEntityCode(resumeName, EntityType.Resume);

    if (!userId)
      return await this.resumeRepository.queryResume(resumeCode, fieldCode);

    return await this.resumeRepository.queryResumeByUser(
      resumeCode,
      fieldCode,
      userId,
    );
  }

  // return resume by id in current user
  async getResumeByIdInUser(resumeId: string, userId: string): Promise<Resume> {
    return await this.resumeRepository.findOne({
      where: {
        id: resumeId,
        user: { id: userId },
        isDelete: IsDeleteEntity.False,
      },
      relations: ReturnResumeRelations,
    });
  }

  // return resume by id
  async getResumeById(resumeId: string): Promise<Resume> {
    return await this.resumeRepository.findOne({
      where: { id: resumeId },
      relations: ReturnResumeRelations,
    });
  }
  // // query Resume by field
  // async queryResumeByField(fieldName: string): Promise<Resume[]> {
  //   return await this.resumeRepository.createQueryBuilder('Resume')
  //     .leftJoinAndSelect('Resume.field', 'field')
  //     .where('field.fieldCode =:fieldName', {
  //       fieldName: NameFieldCode(fieldName),
  //     })
  //     .andWhere('Resume.isDelete = false')
  //     .getMany();
  // }
  // // get Resume by name
  // async getResumeByName(nameResume: string): Promise<Resume> {
  //   const Resume = await this.resumeRepository.findOne({
  //     where: { nameResume },
  //     relations: ['field', 'answerQuestions'],
  //   });
  //   if (!Resume) throw new NotFoundException();
  //   return Resume;
  // }

  // restore Resume
  async restoreResume(resumeId: string): Promise<ReturnNotification> {
    const resume = await this.resumeRepository.getDetetedResume(resumeId);
    if (!resume) throw new NotFoundException();
    await this.resumeRepository.update(resumeId, {
      isDelete: IsDeleteEntity.False,
    });
    return {
      message: ReturnStatus.Restore,
      value: resume,
    };
  }
  // hard delete Resume
  async hardDeleteResume(resumeId: string): Promise<ReturnNotification> {
    // check Resume exist in recycle
    const resume = await this.resumeRepository.findOne({
      where: { id: resumeId, isDelete: IsDeleteEntity.True },
      relations: ResumeDeleteRelation,
    });

    if (!resume) throw new NotFoundException();
    console.log(resume.answerQuestions);
    // remove all questions in resume
    await this.answerRepository.delete({ resume: resume });
    await this.resumeRepository.remove(resume);
    return { message: ReturnStatus.Delete, value: resume };
  }

  // // get Resume in user
  // //   async getResumeInUser(ResumeId: string, userId: string): Promise<Resume> {
  // //     return await this.resumeRepository.getResumeInUser(ResumeId, userId);
  // //   }

  // get question havent answer in Resume
  async getQuestionNotAnswer(
    resumeId: string,
    userId: string,
  ): Promise<Question[]> {
    const resume = await this.getResumeByIdInUser(resumeId, userId);
    if (!resume) throw new NotFoundException();
    const questions = await this.questionRepository
      .createQueryBuilder('question')
      .leftJoinAndSelect(
        'question.answerQuestion',
        'answer',
        'answer.answerQuestion=""',
      )
      .leftJoin('answer.resume', 'resume')
      .where('resume.isDelete = false')
      .andWhere('resume.id=:resumeId', { resumeId })
      .getMany();
    return questions;
  }

  // // async getResumeUser(id: string) {
  // //   const Resumes = await this.resumeRepository.getResumeUser(id);
  // //   const reResumes = [];
  // //   for (let i = 0; i < Resumes.length; i++) {
  // //     const reResume = ReturnFormResume(Resumes[i]);
  // //     reResumes.push(reResume);
  // //   }
  // //   return reResumes;
  // // }
  // //   async getResumeUser(id: string): Promise<FromResumeDTO[]> {
  // //     const Resumes = await this.resumeRepository.getResumeUser(id);
  // //     const reResumes = [];
  // //     for (let i = 0; i < Resumes.length; i++) {
  // //       const reResume = ReturnFormResume(Resumes[i]);
  // //       reResumes.push(reResume);
  // //     }
  // //     return reResumes;
  // //   }

  // // return field by field id
  // async returnField(fieldId: string): Promise<Field> {
  //   return await this.fieldsService.getFieldById(fieldId);
  // }
  // // update field and answer Resume
  // async updateFieldAnswerResume(
  //   ResumeId: string,
  //   _field: Field,
  //   _answers: AnswerQuestion[],
  // ): Promise<any> {
  //   await this.resumeRepository.update(ResumeId, {
  //     field: _field,
  //     answerQuestions: _answers,
  //   });
  //   return {
  //     message: ReturnStatus.Success,
  //     Resume: await this.resumeRepository.findOne({ id: ResumeId }),
  //   };
  // }
  // async updateResume(userId: string, oldFieldId: string, newFieldId: string) {
  //   // const Resume = await this.resumeRepository.
  //   // console.log(Resume);
  //   const oldField = await this.fieldsService.getFieldById(oldFieldId);
  //   if (!oldField) throw new NotFoundException('Old field ID not found');
  //   const newField = await this.fieldsService.getFieldById(newFieldId);
  //   if (!newField) throw new NotFoundException('New field ID not found');
  //   return this.resumeRepository.updateResume(userId, oldFieldId, newField);
  // }
  // async deleteResume(userId: string, fieldId: string) {
  //   // const Resume = await this.resumeRepository.
  //   // console.log(Resume);
  //   const field = await this.fieldsService.getFieldById(fieldId);
  //   if (!field) throw new NotFoundException('Field ID not found');
  //   return this.resumeRepository.deleteResume(userId, fieldId);
  // }

  // // soft delete Resume
  async softDeleteResume(
    userId: string,
    resumeId: string,
  ): Promise<ReturnNotification> {
    const resume = await this.resumeRepository.getResumeByUser(
      userId,
      resumeId,
    );
    if (!resume) throw new NotFoundException();
    await this.resumeRepository.update(resumeId, {
      isDelete: IsDeleteEntity.True,
    });
    return { message: ReturnStatus.Delete, value: resume };
  }

  // get Resume sofdeteled
  async getResumesSoftDelete(userId: string): Promise<Resume[]> {
    const user = await this.userRepository.findOne({ id: userId });
    return await this.resumeRepository.find({
      where: { isDelete: IsDeleteEntity.True, user },
      relations: ReturnResumeRelations,
    });
  }

  // update answer from question Resume
  async updateAnswer(
    userId: string,
    resumeId: string,
    answerId: string,
    newAnswer: string,
  ): Promise<any> {
    const answer = await this.answerRepository.findOne({
      where: { id: answerId, resume: { id: resumeId, user: { id: userId } } },
    });
    if (!answer) throw new NotFoundException();
    await this.answerRepository.update(answerId, {
      answerQuestion: newAnswer,
    });

    return await this.resumeRepository.findOne({
      where: { id: resumeId },
      relations: ReturnResumeRelations,
    });
  }

  // delete answer by id from Resume
  async deleteAnswer(
    userId: string,
    resumeId: string,
    answerId: string,
  ): Promise<any> {
    const user = await this.userRepository.findOne({ id: userId });
    const answer = await this.answerRepository.findOne({
      where: {
        id: answerId,
        resume: { id: resumeId, isDelete: IsDeleteEntity.False, user },
      },
    });
    if (!answer) throw new NotFoundException();
    await this.answerRepository.update(answerId, { answerQuestion: '' });
    return { message: ReturnStatus.Delete, answer: answer.answerQuestion };
  }
  // // get answer by id
  // async getAnswerById(answerId: string): Promise<AnswerQuestion> {
  //   return await this.answerRepository
  //     .createQueryBuilder('answer')
  //     .leftJoinAndSelect('answer.ResumeEntity', 'ResumeEntity')
  //     .where('answer.id=:answerId', { answerId })
  //     .andWhere('ResumeEntity.isDelete = false')
  //     .getOne();
  // }
  async getAnswer(user) {
    const topic = await this.topicQuestion
      .createQueryBuilder('topic')
      .where('topic.typeTopic= :typeTopic', { typeTopic: 'common' })
      .orderBy('topic.createAt', 'ASC')
      .getMany();
    const obj = [];
    const questions = await this.questionRepository.find({
      where: { field: IsNull() },
      relations: ['topicQuestion'],
    });
    const listAnswer = await this.answerRepository
      .createQueryBuilder('answerQuestion')
      .leftJoinAndSelect('answerQuestion.user', 'user')
      .leftJoinAndSelect('answerQuestion.question', 'question')
      .where('answerQuestion.user = :user', {
        user: user.userId,
      })
      .getMany();
    topic.map((topic) => {
      const container = [topic.topicQuestion];
      questions.map((question) => {
        let container1 = '';
        listAnswer.map((answer) => {
          if (
            question.id === answer.question.id &&
            question.topicQuestion.topicQuestion === topic.topicQuestion
          ) {
            container1 += `${question.titleQuestion} ${answer.answerQuestion}`;
          }
        });
        if (container1) {
          container.push(container1);
        }
      });
      obj.push(container);
    });
    let container = '';
    for (let i = 0; i < obj.length; i++) {
      let box = '';
      obj[i].map((item) => {
        box += `${item}\n\t`;
      });
      container += `${box}\n`;
    }
    return container;
  }
  async getAnswerSeparate(nameResume) {
    const resume = await this.resumeRepository
      .createQueryBuilder('resume')
      .leftJoinAndSelect('resume.field', 'field')
      .where('resume.nameResume = :nameResume', { nameResume: nameResume })
      .getOne();
    const listAnswer = await this.answerRepository
      .createQueryBuilder('answer')
      .leftJoinAndSelect('answer.resume', 'resume')
      .leftJoinAndSelect('answer.question', 'question')
      .where('answer.resume = :resume ', { resume: resume.id })
      .orderBy('answer.createAt', 'ASC')
      .getMany();
    const questions = await this.questionRepository.find({
      where: { field: resume.field.id },
      relations: ['topicQuestion'],
    });
    const topics = await this.topicQuestion
      .createQueryBuilder('topic')
      .where('topic.typeTopic= :typeTopic', { typeTopic: 'seperate' })
      .orderBy('topic.createAt', 'ASC')
      .getMany();
    // console.log(questions);
    const box = [];
    topics.map((topic) => {
      const container = [topic.topicQuestion];
      listAnswer.map((answer) => {
        let container1 = '';
        questions.map((question) => {
          if (
            question.id === answer.question.id &&
            question.topicQuestion.topicQuestion === topic.topicQuestion
          ) {
            container1 += `${question.titleQuestion} ${answer.answerQuestion}`;
          }
        });
        if (container1) {
          container.push(container1);
        }
      });
      if (container.length > 1) {
        box.push(container);
      }
    });
    let container = '';
    for (let i = 0; i < box.length; i++) {
      let container1 = '';
      box[i].map((item) => {
        container1 += `${item}\n\t`;
      });
      container += `${container1}\n`;
    }
    return container;
  }

  async getTotalTopic(user): Promise<any> {
    return this.resumeRepository.find({ user: user.userId });
  }
  async getListID(id: string) {
    return this.resumeRepository.getResumeUser(id);
  }
  async printPdf(informationResume, user, nameResume, res) {
    const inforUser = await this.userRepository.findOne({ id: user.userId });
    const inforResume = await this.resumeRepository.findOne({
      nameResume: nameResume,
    });
    console.log(informationResume);
    const fonts = {
      Helvetica: {
        normal: 'Helvetica',
        bold: 'Helvetica-Bold',
        italics: 'Helvetica-Oblique',
        bolditalics: 'Helvetica-BoldOblique',
      },
    };
    const printer = new PdfPrinter(fonts);
    const docDefinition = {
      content: [
        {
          // image: 'uploads/user-avatar/pii.jpg', //`${inforResume.imageResume}`,
          image: `${inforResume.imageResume}`,
          width: 100,
          height: 100,
          style: { alignment: 'center' },
        },
        {
          text: `${inforUser.fullName}`,
          margin: [0, 8, 0, 0],
          style: ['header', 'anotherStyle'],
          lineHeight: 1.2,
        },
        {
          text: `${nameResume}`,
          style: ['anotherStyle'],
          margin: [0, 0, 0, 10],
          fontSize: 16,
          lineHeight: 1.2,
        },
        {
          text: `${informationResume.toString()}`,
        },
      ],
      defaultStyle: {
        font: 'Helvetica',
        margin: [0, 3, 0, 3],
        lineHeight: 1.8,
      },
      styles: {
        header: {
          fontSize: 25,
          bold: true,
        },
        anotherStyle: {
          italics: true,
          alignment: 'center',
        },
      },
    };
    const options = {};
    let test;
    const file_name = 'PDF' + Date.now() + '.pdf';
    const pdfDoc = printer.createPdfKitDocument(docDefinition, options);
    pdfDoc.pipe((test = fs.createWriteStream('uploads/pdf/' + file_name)));
    pdfDoc.end();
    test.on('finish', async function () {
      res.download('uploads/pdf/' + file_name);
    });
  }

  // async getAnswer(user) {
  //   const topic = await this.topicQuestion.find({ typeTopic: 'common' });
  //   const obj = [];
  //   const questions = await this.questionRepository.find({
  //     where: { field: IsNull() },
  //     relations: ['topicQuestion'],
  //   });
  //   const listAnswer = await this.answerRepository
  //     .createQueryBuilder('answerQuestion')
  //     .leftJoinAndSelect('answerQuestion.user', 'user')
  //     .leftJoinAndSelect('answerQuestion.question', 'question')
  //     .where('answerQuestion.user = :user', {
  //       user: user.userId,
  //     })
  //     .getMany();
  //   // console.log(topic);
  //   topic.map((topic) => {
  //     const container = [{ name: topic.topicQuestion }];
  //     questions.map((question) => {
  //       let container1;
  //       listAnswer.map((answer) => {
  //         if (
  //           question.id === answer.question.id &&
  //           question.topicQuestion.topicQuestion === topic.topicQuestion
  //         ) {
  //           container1 = new Object();
  //           container1['idQuestion'] = question.id;
  //           container1['nameQuestion'] = question.contentQuestion;
  //           container1['idAnswer'] = answer.id;
  //           container1['contentAnswer'] = answer.answerQuestion;
  //         }
  //       });
  //       if (container1) {
  //         container.push(container1);
  //       }
  //     });
  //     obj.push(container);
  //   });
  //   console.log(obj);
  //   // return obj;
  // }
}
