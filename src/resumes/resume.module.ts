import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
import { Field } from 'src/fileds/entities/field.entity';
import { Question } from 'src/questions/entities/questions.entity';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { User } from 'src/users/entities/user.entity';
import { ResumesController } from './resume.controller';
import { ResumeRepository } from './resume.repository';
import { ResumeService } from './resume.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ResumeRepository,
      AnswerQuestion,
      User,
      Field,
      Question,
      TopicQuestion,
    ]),
  ],
  controllers: [ResumesController],
  providers: [ResumeService],
  exports: [ResumeService],
})
export class ResumeModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // consumer.apply()
  }
}
