import { Field } from 'src/fileds/entities/field.entity';
import { DestType } from 'src/model/multer-option';
import {
  ReturnResumeFormDTO,
  TopicAnswerQuestion,
} from '../dtos/return-resume.dto';
import { Resume } from '../entities/resume.entity';

export const FieldList = {
  IT: 'IT',
  Banking: 'Banking',
  Accounting: 'Accounting',
  Marketing: 'Marketing',
  Chef: 'Chef',
};
// export const ListField = ['IT', 'Banking', 'Account', 'Chef'];
export const ReturnResumeRelations = [
  'field',
  // 'field.questions.answerQuestion',
  'answerQuestions',
  // 'user',
  'answerQuestions.question',
  // 'answerQuestions.question.topicQuestion',
];

export const ResumeDeleteRelation = ['answerQuestions'];

export const ReturnResumeImgPath = (filename: string) => {
  return DestType.CV + Date.now() + filename;
};

export const ReturnFormResume = (resume: Resume) => {
  const topicAnswerQuestions = resume.answerQuestions.map((answer) => {
    return new TopicAnswerQuestion(
      answer.question.topicQuestion.topicQuestion,
      answer.question.contentQuestion,
      answer.answerQuestion,
    );
  });
  return new ReturnResumeFormDTO(
    resume.nameResume,
    resume.resumeVersion,
    resume.imageResume,
    resume.field.nameField,
    topicAnswerQuestions,
  );
};

export enum TopicAnswer {}

export const ITHardQuestionList = [
  { content: 'How much project did you make?', title: 'experience' },
  { content: 'How many members in your team?', title: 'project' },
  {
    content: 'Show me list of Technology you used in project',
    title: 'experience',
  },
  { content: 'What are Language you know?', title: 'knowledge' },
];

export const DeleteAnswer = {
  True: true,
  False: false,
};
