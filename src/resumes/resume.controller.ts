import {
  Body,
  Get,
  Controller,
  Query,
  Res,
  Param,
  UploadedFile,
  Post,
  UseInterceptors,
  Patch,
  Delete,
  UseGuards,
  NotFoundException,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiQuery,
  ApiBody,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CurrentUser } from 'src/decorators/currentUser.decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import { Cookies } from 'src/decorators/cookies';
import { CookieNames, ReturnStatus } from 'src/auth/constants/constants';
import { ResumeService } from './resume.service';
import { CurrentUserDTO } from 'src/auth/dtos/auth.dto';
import { Resume } from './entities/resume.entity';
import { DestType, multerOptions } from 'src/model/multer-option';
import {
  AnswerEmptyQuestionDTO,
  AnswerQuestionDTO,
} from 'src/answerQuestions/dtos/answer-question.dto';
import { query, Response } from 'express';
import { Question } from 'src/questions/entities/questions.entity';
import { CreateResumeDTO } from './dtos/create-resume.dto';
import { FormBodySwagger } from 'src/constants/swagger.constants';
import { ReturnNotification } from 'src/constants/entity.constants';
import {
  AnswerResumeDTO,
  AnswerResumeObjectDTO,
  UpdateAnswerResumeDTO,
} from './dtos/answer-resume.dto';
import { QueryResumeDTO } from './dtos/query-resume.dto';
import { UpdateResumeDTO } from './dtos/update-resume.dto';
import { DeleteAnswer } from './constants/resume.constant';

// import { Roles } from 'src/users/dto/roles.decorator';
// import { Fields } from 'src/fileds/entities/field.entity';

@Controller('/resume')
@ApiBearerAuth('access-token')
@ApiTags('Resumes')
@UseGuards(JwtAuthGuard)
export class ResumesController {
  constructor(private resumesServices: ResumeService) {}

  // create resume
  @Post('create')
  @ApiOperation({
    summary: 'Create Resume in current user with field',
  })
  @ApiConsumes(FormBodySwagger.FormData)
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.CV)))
  async createResume(
    @CurrentUser() user: CurrentUserDTO,
    @Body() newResume: CreateResumeDTO,
    @UploadedFile() file: Express.Multer.File,
    @Res({ passthrough: true }) res: Response,
  ): Promise<ReturnNotification> {
    try {
      const resume = await this.resumesServices.createResume(
        user.userId,
        newResume,
        file.filename,
      );
      res.cookie(CookieNames.ResumeId, resume.id);
      res.cookie(CookieNames.Questions, resume.field.questions);
      res.cookie(CookieNames.FieldId, resume.field.id);
      return { message: ReturnStatus.Create, value: resume };
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.CREATED);
    }
  }
  // confirm Resume
  @Post('submit')
  @ApiOperation({
    summary: 'Answer questions follow field in Resume created by user',
  })
  @ApiConsumes(FormBodySwagger.FormJson)
  async confirmResume(
    @Cookies(CookieNames.ResumeId) resumeId: string,
    @Cookies(CookieNames.FieldId) fieldId: string,
    @Cookies(CookieNames.Questions) questions: Question[],
    @Body() answers: AnswerResumeDTO,
  ): Promise<ReturnNotification> {
    console.log(answers);
    console.log(answers.answers[0]);
    console.log(answers.answers[0].answerQuestion[0]);

    const resume = await this.resumesServices.answerAndSubmitResume(
      resumeId,
      // fieldId,
      questions,
      answers.answers,
      DeleteAnswer.False,
    );

    return {
      message: ReturnStatus.Create,
      value: resume,
    };
  }

  // get resume in current user
  @ApiOperation({ summary: 'Get all current user Resume' })
  @Get('user')
  async getResumeUser(
    @CurrentUser() user: CurrentUserDTO,
    @Res({ passthrough: true }) res: Response,
  ): Promise<ReturnNotification> {
    const resume = await this.resumesServices.getResumeCurrentUser(user.userId);
    res.cookie(CookieNames.UserInformation, resume);
    return { message: ReturnStatus.Success, value: resume };
  }

  // get Resume by Resume name of all user
  @Get('all/user')
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @ApiOperation({ summary: 'Get Resume by Resume name or field of all user' })
  @ApiConsumes()
  async getResumeByName(@Query() query: QueryResumeDTO) {
    return await this.resumesServices.queryResume(
      query.fieldName,
      query.nameResume,
    );
  }

  // get Resume by Resume name of  user
  @Get('resume')
  @ApiOperation({
    summary: 'Get Resume by Resume name or field of current user',
  })
  async getResumeByNameInUser(
    @CurrentUser() user: CurrentUserDTO,
    @Query() query: QueryResumeDTO,
  ) {
    return await this.resumesServices.queryResume(
      query.fieldName,
      query.nameResume,
      user.userId,
    );
  }

  // get resume by id in current user
  @Get('resume/:id')
  @ApiOperation({ summary: 'Get Resumes by Id in Current User' })
  async getResumeByIdUser(
    @Param('id') id: string,
    @CurrentUser() user: CurrentUserDTO,
    @Res({ passthrough: true }) res: Response,
  ): Promise<Resume> {
    const resume = await this.resumesServices.getResumeByIdInUser(
      id,
      user.userId,
    );
    if (!resume) throw new NotFoundException();

    res.cookie(CookieNames.ResumeId, resume.id);
    return resume;
  }

  // get resume by id
  // @Get('resume/all/:id')
  // @ApiOperation({
  //   summary: 'Get resume by id in all user',
  // })
  // async getResumeById(@Param('id') id: string): Promise<Resume> {
  //   return await this.resumesServices.getResumeById(id);
  // }

  // soft delete Resume
  @Patch('delete/:idResume')
  @ApiOperation({ summary: 'Soft delete Resume in user by id' })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async softDeleteResume(
    @CurrentUser() user: CurrentUserDTO,
    @Param('idResume') idResume: string,
  ): Promise<ReturnNotification> {
    return await this.resumesServices.softDeleteResume(user.userId, idResume);
  }

  // get recycle bin
  @Get('recycle-bin')
  @ApiOperation({ summary: 'Get all Resume soft deleted in Recycle Bin' })
  async getResumeRecycle(
    @CurrentUser() user: CurrentUserDTO,
  ): Promise<Resume[]> {
    return await this.resumesServices.getResumesSoftDelete(user.userId);
  }

  // restore resume in recycle bin
  @Patch('recycle-bin/:id')
  @ApiOperation({
    summary: 'Restore Resume soft deleted in Recycle Bin by id',
  })
  async restoreResume(@Param('id') id: string): Promise<ReturnNotification> {
    return await this.resumesServices.restoreResume(id);
  }

  // hard delete Resume in recycle bin
  @Delete('recycle-bin/:id')
  @ApiOperation({ summary: 'Delete Resume in Recycle Bin' })
  async hardDelResume(@Param('id') id: string): Promise<ReturnNotification> {
    console.log(id);

    return await this.resumesServices.hardDeleteResume(id);
  }

  // update resume img
  @Patch('update')
  @ApiOperation({
    summary: 'Update Name, Version, Img Resume',
  })
  @ApiConsumes(FormBodySwagger.FormData)
  @ApiBearerAuth('access-token')
  @UseInterceptors(FileInterceptor('photo', multerOptions(DestType.CV)))
  async update(
    @CurrentUser() user: CurrentUserDTO,
    @UploadedFile() file: Express.Multer.File,
    @Body() updateUserDto: UpdateResumeDTO,
    @Cookies(CookieNames.ResumeId) resumeId: string,
  ): Promise<ReturnNotification> {
    const resume = await this.resumesServices.updateResume(
      resumeId,
      // user.userId,
      updateUserDto,
      file,
    );
    return { message: ReturnStatus.Update, value: resume };
  }

  // get question havent answer in Resume
  @Get('get/question')
  @ApiOperation({
    summary: 'Get questions which havent answer in Resume by id',
  })
  async getQuestionNotAnswer(
    @Cookies(CookieNames.ResumeId) resumeId: string,
    @CurrentUser() user: CurrentUserDTO,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any[]> {
    const questions = await this.resumesServices.getQuestionNotAnswer(
      resumeId,
      user.userId,
    );
    // res.cookie(CookieNames.ResumeId, resumeId);
    res.cookie(CookieNames.Questions, questions);
    return questions;
  }

  // answer the question havent answer in Resume
  @Post('question/answer')
  @ApiOperation({
    summary: 'Answer questions which havent answer in Resume getted',
  })
  @ApiConsumes(FormBodySwagger.FormJson)
  async answerQuestionNotAnswer(
    @CurrentUser() user: CurrentUserDTO,
    @Cookies(CookieNames.ResumeId) resumeId: string,
    @Cookies(CookieNames.Questions) questions: Question[],
    @Body() answers: AnswerResumeDTO,
  ): Promise<ReturnNotification> {
    const resume = await this.resumesServices.answerAndSubmitResume(
      resumeId,
      questions,
      answers.answers,
      DeleteAnswer.True,
    );
    return {
      message: ReturnStatus.Create,
      value: resume,
    };
  }

  // @Patch('delete/field=:fieldId')
  // @ApiConsumes(FormBodySwagger.FormUrlencoded)
  // async deleteResume(
  //   @CurrentUser() user: any,
  //   @Param('fieldId') fieldId: string,
  // ) {
  //   return await this.ResumesServices.deleteResume(user.userId, fieldId);
  // }

  // update answer from question from Resume
  @Patch('update/resume/:answerId')
  @ApiOperation({
    summary: 'Update answer questions in user by Resume id by answer id',
  })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async updateAnswer(
    @CurrentUser() user: CurrentUserDTO,
    @Cookies(CookieNames.ResumeId) resumeId: string,
    @Param('answerId') answerId: string,
    @Body() answer: UpdateAnswerResumeDTO,
  ): Promise<any> {
    return await this.resumesServices.updateAnswer(
      user.userId,
      resumeId,
      answerId,
      answer.answerQuestion,
    );
  }

  // delete answer from Resume
  @Patch('delete/resume/:answerId')
  @ApiOperation({ summary: 'Delete answer in user by Resume by answer id' })
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  async deleteAnswer(
    @Cookies(CookieNames.ResumeId) resumeId: string,
    @CurrentUser() user: CurrentUserDTO,
    @Param('answerId') answerId: string,
  ): Promise<Resume> {
    return await this.resumesServices.deleteAnswer(
      user.userId,
      resumeId,
      answerId,
    );
  }

  @Get('get-total-answer:nameResume')
  @ApiOperation({ summary: 'Get total answers for resume choosed' })
  async getAnswer(
    @CurrentUser() user,
    @Param('nameResume') nameResume: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    const answersCommon = await this.resumesServices.getAnswer(user);
    const answersSeparate = await this.resumesServices.getAnswerSeparate(
      nameResume,
    );
    const informationResume = answersCommon + answersSeparate;
    res.cookie(CookieNames.InformationResume, informationResume);
    res.cookie(CookieNames.NameResume, nameResume);
    return 'Get information for resume successfully';
  }

  // @Get('List/idResume')
  // async GetList(@CurrentUser() user) {
  //   return await this.ResumesServices.getListID(user.userId);
  // }

  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @Post('infor-answer/export-to-cv')
  async getTotalAnswerCommon(
    @CurrentUser() user: any,
    @Res() res: Response,
    @Cookies(CookieNames.InformationResume) informationResume: any,
    @Cookies(CookieNames.NameResume) nameResume: any,
  ) {
    console.log(nameResume);
    await this.resumesServices.printPdf(
      informationResume,
      user,
      nameResume,
      res,
    );
  }

  // @Post('create/field')
  // async createField() {
  //   await this.resumesServices.createField();
  // }

  // @Get('topic')
  // @ApiOperation({ summary: 'Get name resume of user' })
  // async getTotalTopic(
  //   @CurrentUser() user,
  //   @Res({ passthrough: true }) res: Response,
  // ): Promise<any> {
  //   const listResumes = await this.resumesServices.getTotalTopic(user);
  //   const listNameResumes = [];
  //   await listResumes.map((resume) => {
  //     listNameResumes.push(resume.nameResume);
  //   });
  //   res.cookie('listNameResumes', listNameResumes);
  //   return listNameResumes;
  // }

  // @Get('topic-fields')
  // async get
  @Get('topic')
  async getTotalTopic(
    @CurrentUser() user,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    const listResumes = await this.resumesServices.getTotalTopic(user);
    const listNameResumes = [];
    await listResumes.map((resume) => {
      listNameResumes.push(resume.nameResume);
    });
    res.cookie(CookieNames.ListNameResumes, listNameResumes);
    return listNameResumes;
  }
}
