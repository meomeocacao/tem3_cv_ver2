import { ApiProperty } from '@nestjs/swagger';
import { Field } from 'src/fileds/entities/field.entity';
import { User } from 'src/users/entities/user.entity';
import { Resume } from '../entities/resume.entity';
import { FieldList } from 'src/constants/field.constant';

export class ResumeDTO {
  // @ApiProperty({ type: 'enum', enum: Fields, default: Fields.IT })
  // field: Fields;

  user: User;

  field: Field;
}

export class TextDto {
  @ApiProperty({
    description: 'Name of Resume',
    default: 'Resume IT BackEnd',
  })
  nameResume: string;
  @ApiProperty({
    description: 'Name of Field',
    default: 'IT',
    type: 'enum',
    enum: FieldList,
  })
  text: string;
}

export class FindNameResumeDTO {
  @ApiProperty({
    description: 'Name Resume to find',
    default: 'Resume IT BackEnd',
  })
  nameResume: string;
}

export class QueryResumeByFieldDTO {
  @ApiProperty({
    description: 'Name field to query',
    default: 'IT',
  })
  nameResume: string;
}

export class FromResumeDTO {
  id: string;
  field: string;
  answer: FormAnswerDTO[];
}

export class FormAnswerDTO {
  constructor(
    _topic: string,
    _question: string,
    _answer: string,
    _answerId: string,
  ) {
    this.topic = _topic;
    this.question = _question;
    this.answer = _answer;
    this.answerId = _answerId;
  }
  topic: string;
  question: string;
  answer: string;
  answerId: string;
}

// export const ReturnFormResume = (Resume: Resume): FromResumeDTO => {
//   const _reResume = new FromResumeDTO();
//   _reResume.id = Resume.id;
//   _reResume.field = Resume.field.nameField;
//   _reResume.answer = [];
//   for (let i = 0; i < Resume.answerQuestions.length; i++) {
//     const _topic =
//       Resume.answerQuestions[i].question.topicQuestion.topicQuestion;
//     const _question = Resume.answerQuestions[i].question.contentQuestion;
//     const _answer = Resume.answerQuestions[i].answerQuestion;
//     const _answerId = Resume.answerQuestions[i].id;
//     const reAnswer = new FormAnswerDTO(_topic, _question, _answer, _answerId);
//     _reResume.answer.push(reAnswer);
//   }
//   return _reResume;
// };

export class FiledDTO {
  @ApiProperty({ type: 'enum', enum: FieldList })
  fields: string;
}
