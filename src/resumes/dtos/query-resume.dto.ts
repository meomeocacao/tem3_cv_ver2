import { ApiProperty } from '@nestjs/swagger';
import { FieldList } from '../constants/resume.constant';

export class QueryResumeDTO {
  @ApiProperty({
    type: 'string',
    description: 'Name of resume',
    required: false,
  })
  nameResume?: string;
  @ApiProperty({
    type: 'enum',
    description: 'Name of Field',
    required: false,
    default: FieldList.IT,
    enum: FieldList,
  })
  fieldName?: string;
}
