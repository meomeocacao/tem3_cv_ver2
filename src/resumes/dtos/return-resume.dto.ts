export class ReturnResumeFormDTO {
  constructor(
    name: string,
    version: string,
    imgPath: string,
    field: string,
    topicAnswerQuestions: TopicAnswerQuestion[],
  ) {
    this.name = name;
    this.version = version;
    this.imgPath = imgPath;
    this.field = field;
    this.topicAnswerQuestions = topicAnswerQuestions;
  }
  name: string;
  version: string;
  imgPath: string;
  field: string;
  topicAnswerQuestions: TopicAnswerQuestion[];
}

export class TopicAnswerQuestion {
  constructor(topic: string, question: string, answer: string) {
    this.topic = topic;
    this.question = question;
    this.answer = answer;
  }
  topic: string;
  question: string;
  answer: string;
}
