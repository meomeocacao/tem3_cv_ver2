import { ApiProperty } from '@nestjs/swagger';
import { Question } from 'src/questions/entities/questions.entity';
export class AnswerResumeDTO {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'object',
      properties: {
        answerQuestion: { type: 'array' },
      },
      default: {
        answerQuestion: ['Cau tra loi', 'Cau tra loi'],
      },
    },
  })
  answers: AnswerResumeObjectDTO[];
}

export class AnswerResumeObjectDTO {
  answerQuestion: string[];
}

export class UpdateAnswerResumeDTO {
  @ApiProperty({
    type: 'string',
    description: 'new answer to update',
    default: 'new answer',
  })
  answerQuestion: string;
}
