import { ApiProperty } from '@nestjs/swagger';

export class UpdateResumeDTO {
  @ApiProperty({
    type: 'string',
    description: 'Rename Resume',
    required: false,
  })
  nameResume?: string;
  @ApiProperty({
    type: 'string',
    description: 'Change version of resume',
    required: false,
  })
  resumeVersion?: string;
  @ApiProperty({
    type: 'file',
    description: ' Upload Img',
    required: false,
  })
  photo?: any;
  imageResume?: string;
}
