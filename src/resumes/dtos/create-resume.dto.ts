import { ApiProperty } from '@nestjs/swagger';
import { FieldList } from '../constants/resume.constant';

export class CreateResumeDTO {
  @ApiProperty({
    type: 'string',
    description: 'Name of resume',
    default: 'Nestjs Intern Resume',
  })
  nameResume: string;
  @ApiProperty({
    type: 'string',
    description: 'Version of resume',
    default: 'ver0.1',
  })
  resumeVersion: string;
  @ApiProperty({
    type: 'enum',
    description: 'Field of resume',
    enum: FieldList,
  })
  fieldName: string;

  @ApiProperty({
    type: 'file',
    description: 'Image of Resume',
  })
  photo?: any;

  imageResume: string;
}
