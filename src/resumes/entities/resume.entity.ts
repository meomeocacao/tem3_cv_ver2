import { BaseEntity } from 'src/model/base.entity';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
// import { AnswerQuestionCommon } from 'src/answerQuestionsCommon/entities/answerQuestionCommon.entity';
import { Field } from 'src/fileds/entities/field.entity';
import { User } from 'src/users/entities/user.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  VersionColumn,
} from 'typeorm';
import { EntityType, NameEntityCode } from 'src/constants/entity.constants';

@Entity()
export class Resume extends BaseEntity {
  @Column()
  nameResume: string;

  @Column()
  resumeCode: string;

  @Column()
  resumeVersion: string;

  @Column()
  imageResume: string;

  @VersionColumn()
  version: string;

  @ManyToOne(() => User, (user) => user.resume)
  user: User;

  @ManyToOne(() => Field, (field) => field.resume, {
    cascade: true,
  })
  field: Field;

  @OneToMany(() => AnswerQuestion, (answerQuestion) => answerQuestion.resume)
  answerQuestions: AnswerQuestion[];

  @BeforeInsert()
  generateResumeCode() {
    this.resumeCode = NameEntityCode(this.nameResume, EntityType.Resume);
  }
}
