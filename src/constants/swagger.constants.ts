export const FormBodySwagger = {
  FormUrlencoded: 'application/x-www-form-urlencoded',
  FormData: 'multipart/form-data',
  FormJson: 'application/json',
};
