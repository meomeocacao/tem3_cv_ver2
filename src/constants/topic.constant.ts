export enum TopicsCommon {
  Contact = 'Contact',
  Education = 'Education',
  Certification = 'Certification',
}

export enum Topics {
  Summary = 'Summary',
  Skills = 'Skills',
  Experiences = 'Experiences',
  Projects = 'Projects',
  Other = 'Other',
}

export enum TypeTopic {
  Common = 'Common',
  Separate = 'Separate',
}

export const TopicsArr = [
  Topics.Summary,
  Topics.Skills,
  Topics.Experiences,
  Topics.Projects,
  Topics.Other,
];

export const TopicsArrCommon = [
  TopicsCommon.Contact,
  TopicsCommon.Education,
  TopicsCommon.Certification,
];
