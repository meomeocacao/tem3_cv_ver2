import { type } from 'os';
import { AnswerQuestion } from 'src/answerQuestions/entities/answerQuestion.entity';
import { Field } from 'src/fileds/entities/field.entity';
import { Invite } from 'src/invite/entities/invite.entity';
import { Question } from 'src/questions/entities/questions.entity';
import { Resume } from 'src/resumes/entities/resume.entity';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { User } from 'src/users/entities/user.entity';

export const NameEntityCode = (nameEntity: string, entityType: EntityType) => {
  return entityType + '_' + nameEntity.toLowerCase().replace(/ /g, '_');
};

export enum EntityType {
  User = 'user',
  Resume = 'resume',
  Field = 'field',
  Question = 'question',
  Answer = 'answer',
}

export type EntityList = {
  User: User;
  Resume: Resume;
  Field: Field;
  Invite: Invite;
  Question: Question;
  AnswerQuestion: AnswerQuestion;
  TopicQuestion: TopicQuestion;
};

export const IsDeleteEntity = {
  True: true,
  False: false,
};

export class ReturnNotification {
  message: string;
  value: unknown;
}
