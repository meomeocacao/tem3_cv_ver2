import { Topics, TopicsCommon } from './topic.constant';

export const questionCommon = [
  {
    question: 'Số điện thoại của bạn là gì ?',
    topic: TopicsCommon.Contact,
    titleQuestion: 'PhoneNumber :',
  },
  {
    question: 'Gmail của bạn là gì?',
    topic: TopicsCommon.Contact,
    titleQuestion: 'Gmail :',
  },
  {
    question: 'Bạn được đào tạo từ trường hay cơ sở nào ?',
    topic: TopicsCommon.Education,
    titleQuestion: 'Name University :',
  },
  {
    question: 'Bạn có những chứng chỉ gì?',
    topic: TopicsCommon.Certification,
    titleQuestion: 'Degree :',
  },
];

export const questIt = [
  {
    question:
      'Hãy tóm tắt tiểu sử của bạn và những liên quan đến ngành bạn ứng tuyển ?',
    topic: Topics.Summary,
    nameField: 'IT',
    titleQuestion: null,
  },
  {
    question: 'Bạn biết những ngôn ngữ lập trình nào',
    topic: Topics.Skills,
    nameField: 'IT',
    titleQuestion: 'Program languages : ',
  },
  {
    question: 'Bạn có đã làm dự án nào ?',
    topic: Topics.Projects,
    nameField: 'IT',
    titleQuestion: 'Name Project :',
  },
  {
    question: 'Dự án gồm bao nhiêu thành viên? ',
    topic: Topics.Projects,
    nameField: 'IT',
    titleQuestion: 'Members :',
  },
  {
    question: 'Công nghệ sử dụng trong dự án là gì?',
    topic: Topics.Projects,
    nameField: 'IT',
    titleQuestion: 'Technologies :',
  },
  {
    question: 'Bạn đã làm ở vị trí nào ? ',
    topic: Topics.Experiences,
    nameField: 'IT',
    titleQuestion: 'Position :',
  },
  {
    question: 'Thời gian là bao lâu ? ',
    topic: Topics.Experiences,
    nameField: 'IT',
    titleQuestion: 'Time :',
  },
];
export const questChef = [
  {
    question:
      'Hãy tóm tắt tiểu sử của bạn và những liên quan đến ngành bạn ứng tuyển ?',
    topic: Topics.Summary,
    nameField: 'Chef',
    titleQuestion: '',
  },
  {
    question: 'Bạn đã từng đặt giải thưởng ở cuộc thi lớn nào chưa ?',
    topic: Topics.Other,
    nameField: 'Chef',
    titleQuestion: 'Achivement',
  },
  {
    question: 'Bạn có thể làm những loại món ăn như thế nào ?',
    topic: Topics.Skills,
    nameField: 'Chef',
    titleQuestion: 'Type dish :',
  },
  {
    question: 'Bạn đã có kinh nghiệm làm việc ở đâu chưa ?',
    topic: Topics.Experiences,
    nameField: 'Chef',
    titleQuestion: 'Area :',
  },
];

export const questAcount = [
  {
    question:
      'Hãy tóm tắt tiểu sử của bạn và những liên quan đến ngành bạn ứng tuyển ?',
    topic: Topics.Summary,
    nameField: 'Accounting',
    titleQuestion: '',
  },
  {
    question: 'Bạn có những kĩ năng đặc biệt nào?',
    topic: Topics.Skills,
    nameField: 'Accounting',
    titleQuestion: 'Languages',
  },
  {
    question: 'Bạn đã có kinh nghiệm làm việc ở đâu chưa ?',
    topic: Topics.Experiences,
    nameField: 'Accounting',
    titleQuestion: 'Work Experience :',
  },
  {
    question: 'Bạn đã có từng đặt giải thường gì không ?',
    topic: Topics.Other,
    nameField: 'Accounting',
    titleQuestion: 'Achivement',
  },
];
