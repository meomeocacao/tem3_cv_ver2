import { User } from 'src/users/entities/user.entity';
import { HostEmail, MailerConfig } from './auth/constants/constants';
import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ResumeModule } from './resumes/resume.module';
import { FieldsModule } from './fileds/fields.module';
import { GoogleModule } from './auth/google/google.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { TopicQuestionModule } from './topicsQuestion/topicQuestion.module';
import { AnswerQuestionsModule } from './answerQuestions/answerQuestion.module';
import { FacebookStrategy } from './auth/strategies/facebook.strategy';
import { FacebookModule } from './auth/facebook/facebook.module';
import { InviteModule } from './invite/invite.module';
import { QuestionModule } from './questions/questions.module';

@Module({
  imports: [
    MailerModule.forRoot(MailerConfig),
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot({}),
    TypeOrmModule.forFeature([User]),
    AuthModule,
    ResumeModule,
    FieldsModule,
    GoogleModule,
    FacebookModule,
    TopicQuestionModule,
    AnswerQuestionsModule,
    InviteModule,
    UsersModule,
    QuestionModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    FacebookStrategy,
  ],
})
export class AppModule {}
