import { TypeOrmModule } from '@nestjs/typeorm';
import { AnswerQuestion } from './entities/answerQuestion.entity';
import { AnswerQuestionsService } from './answerQuestion.service';
import { AnswerQuestionController } from './answerQuestion.controller';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';
import { Module } from '@nestjs/common';
import { JwtStrategy } from 'src/auth/strategies/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants/constants';
import { Question } from 'src/questions/entities/questions.entity';
import { Resume } from 'src/resumes/entities/resume.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([AnswerQuestion, TopicQuestion, Question, Resume]),
    JwtModule.register({
      secret: jwtConstants.userSecret,
      signOptions: { expiresIn: jwtConstants.userTime },
    }),
  ],
  controllers: [AnswerQuestionController],
  providers: [AnswerQuestionsService],
  exports: [AnswerQuestionsService],
})
export class AnswerQuestionsModule {}
