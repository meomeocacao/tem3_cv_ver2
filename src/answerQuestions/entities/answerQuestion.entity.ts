import { Resume } from 'src/resumes/entities/resume.entity';
import { Question } from 'src/questions/entities/questions.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'src/model/base.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class AnswerQuestion extends BaseEntity {
  @Column({ nullable: true })
  answerQuestion: string;

  @ManyToOne(() => Question, (question) => question.answerQuestion, {
    cascade: true,
  })
  question: Question;

  @ManyToOne(() => Resume, (Resume) => Resume.answerQuestions, {
    cascade: true,
  })
  resume: Resume;

  @ManyToOne(() => User, (user) => user.answerQuestion)
  user: User;
}
