import {
  Body,
  ConsoleLogger,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Cookies } from 'src/decorators/cookies';
import { CurrentUser } from 'src/decorators/currentUser.decorator';
import { AnswerQuestionsService } from './answerQuestion.service';
import {
  AnswerQuestionDTO,
  AnswerQuestionSeperate,
  CreateAnswerQuestion,
} from './dtos/answer-question.dto';
import { Response } from 'express';
import { CreateTopicQuestionDTO } from 'src/topicsQuestion/dtos/create-topicsQuestion.dto';
import { FormBodySwagger } from 'src/constants/swagger.constants';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CookieNames } from 'src/auth/constants/constants';

@UseGuards(JwtAuthGuard)
@Controller('answer-question')
@ApiBearerAuth('access-token')
@ApiTags('answer-question')
export class AnswerQuestionController {
  constructor(private answerQuestions: AnswerQuestionsService) {}

  // @UseGuards(JwtAuthGuard)
  @Post('result')
  @ApiOperation({ summary: 'answer common questions' })
  async answerQuestion(
    @Cookies(CookieNames.Questions) questions: string[],
    @Body() answer: AnswerQuestionDTO,
    @CurrentUser() user,
  ): Promise<any> {
    return await this.answerQuestions
      .addAnswerQuestion(questions, answer, user)
      .then((result) => {
        return result;
      });
  }

  @ApiTags('Resumes')
  @Get('topic')
  @ApiOperation({ summary: 'Get name resume of user' })
  async getTotalTopic(
    @CurrentUser() user,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    const listResumes = await this.answerQuestions.getTotalTopic(user);
    // console.log(listResumes);
    const listNameResumes = [];

    await listResumes.map((resume) => {
      listNameResumes.push({
        id: resume.id,
        nameResume: resume.nameResume,
        fieldId: resume.field.id,
      });
    });
    res.cookie(CookieNames.ListNameResumes, listNameResumes);
    return listNameResumes;
  }

  @Post('get-question-seperate:resumeName')
  @ApiConsumes(FormBodySwagger.FormUrlencoded)
  @ApiOperation({ summary: 'get seperate questions' })
  async getQuestionSeperate(
    @Body() topicQuestion: CreateTopicQuestionDTO,
    @Param('resumeName') resumeName: string,
    @Cookies(CookieNames.ListNameResumes) listNameResumes,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    let check = false;
    let inforResume;
    listNameResumes.map((resume) => {
      if (resume.nameResume === resumeName) {
        inforResume = resume;
        check = true;
      }
    });
    if (check === false) {
      return 'this resume is not found';
    }
    // console.log(topicQuestion);
    const listQuestion = await this.answerQuestions.getListQuestion(
      resumeName,
      topicQuestion,
      inforResume,
    );
    res.cookie(CookieNames.InforResume, inforResume);
    res.cookie(CookieNames.ListQuestion, listQuestion);
    return listQuestion;
  }

  @Post('answer-question-separate:action')
  @ApiConsumes(FormBodySwagger.FormJson)
  @ApiOperation({ summary: 'answer separate questions' })
  async answerQuestionSeperate(
    @Cookies(CookieNames.InforResume) inforResume,
    @Cookies(CookieNames.ListQuestion) listQuestion,
    @Param('action') action: string,
    @Body() answer: CreateAnswerQuestion,
  ) {
    // console.log(nameTopic);
    const result = await this.answerQuestions.addOrUpdateAnswerQuestion(
      inforResume,
      listQuestion,
      action,
      answer,
    );

    return result;
  }
}
