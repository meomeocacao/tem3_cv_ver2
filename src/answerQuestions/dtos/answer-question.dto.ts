import { ApiProperty } from '@nestjs/swagger';

export class CreateAnswerQuestion {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'object',
      properties: {
        answerQuestion: { type: 'string' },
      },
      default: {
        answerQuestion: 'Cau tra loi',
      },
    },
  })
  answerQuestion: AnswerQuestionDTO[];
}

export class AnswerQuestionDTO {
  @ApiProperty({
    type: String,
    default: ' ',
  })
  answerQuestion: string;
}

export class AnswerEmptyQuestionDTO {
  @ApiProperty({
    type: 'string',
    default: {
      answerQuestion: 'Cau tra loi',
    },
  })
  answerQuestion: string[];
}

export enum actions {
  add = 'add',
  update = 'update',
}

export class typeActions {
  @ApiProperty({ description: 'action in database [  add  ||  update  ]' })
  actions: string;
}

export class AnswerQuestionSeperate {
  @ApiProperty({ isArray: true })
  answerQuestion: string;
}
