import { EntityRepository, Repository } from 'typeorm';
import { AnswerQuestion } from './entities/answerQuestion.entity';

@EntityRepository(AnswerQuestion)
export class AnswerQuestionRepository extends Repository<AnswerQuestion> {}
