import { ConflictException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Resume } from 'src/resumes/entities/resume.entity';
import { ReturnQuestionDTO } from 'src/questions/dtos/create-question.dto';
import { Question } from 'src/questions/entities/questions.entity';
import { QuestionService } from 'src/questions/questions.service';
import { Repository } from 'typeorm';
import {
  AnswerEmptyQuestionDTO,
  AnswerQuestionDTO,
  CreateAnswerQuestion,
} from './dtos/answer-question.dto';
import { AnswerQuestion } from './entities/answerQuestion.entity';
import { ResumeService } from 'src/resumes/resume.service';
import { TopicQuestion } from 'src/topicsQuestion/entities/topicQuestion.entity';

export class AnswerQuestionsService {
  constructor(
    @InjectRepository(AnswerQuestion)
    private answerRepository: Repository<AnswerQuestion>,
    @InjectRepository(TopicQuestion)
    private topicQuestion: Repository<TopicQuestion>,
    @InjectRepository(Question)
    private question: Repository<Question>,
    @InjectRepository(Resume)
    private resume: Repository<Resume>,
  ) {}

  async addAnswerQuestion(questions, data, user) {
    let idQuestion;
    let question;
    for (let i = 0; i < questions.length; i++) {
      const check = await this.answerRepository
        .createQueryBuilder('answerQuestion')
        .leftJoinAndSelect('answerQuestion.user', 'user')
        .where('answerQuestion.question = :question', {
          question: questions[i].id,
        })
        .andWhere('answerQuestion.user = :user', {
          user: user.userId,
        })
        .getOne();

      console.log(check);
      if (check) {
        if (check.user.id === user.userId) {
          continue;
        } else {
          idQuestion = questions[i].id;
          question = await this.question.findOne(questions[i].id);
          break;
        }
      } else {
        idQuestion = questions[i].id;
        question = await this.question.findOne(questions[i].id);
        break;
      }
    }
    if (idQuestion) {
      const aswerQuestion = await this.answerRepository.create({
        answerQuestion: data.answerQuestion,
        question: idQuestion,
        user: user.userId,
      });
      await this.answerRepository.save(aswerQuestion);
      return {
        Cauhoi: question.contentQuestion,
        CauTraLoi: data.answerQuestion,
      };
    } else {
      return 'you do not answer which had exited';
    }
  }
  async getAnswer(user) {
    const topic = await this.topicQuestion.find({ typeTopic: 'common' });
    console.log(topic);
    // const obj = [];
    // const questions =
    //   await this.questionsCommonService.getAllQuestionAndTopic();
    // const listAnswer = await this.answerQuestionRepository
    //   .createQueryBuilder('answerQuestionCommon')
    //   .leftJoinAndSelect('answerQuestionCommon.userEntity', 'user')
    //   .leftJoinAndSelect('answerQuestionCommon.questionsCommon', 'question')
    //   .where('answerQuestionCommon.userEntity = :userEntity', {
    //     userEntity: user.userId,
    //   })
    //   .getMany();
    // // console.log(listAnswer);
    // topic.map((topic) => {
    //   const container = [{ name: topic.topicQuestion }];
    //   questions.map((question) => {
    //     let container1;
    //     listAnswer.map((answer) => {
    //       if (
    //         question.id === answer.questionsCommon.id &&
    //         question.topicQuestionCommon.topicQuestion === topic.topicQuestion
    //       ) {
    //         container1 = new Object();
    //         container1['idQuestion'] = question.id;
    //         container1['nameQuestion'] = question.contentQuestion;
    //         container1['idAnswer'] = answer.id;
    //         container1['contentAnswer'] = answer.answerQuestionCommon;
    //       }
    //     });
    //     if (container1) {
    //       container.push(container1);
    //     }
    //   });
    //   obj.push(container);
    // });
    // return obj;
  }

  async getTotalTopic(user): Promise<any> {
    // return this.resume.find({
    //   where: { userId: user.userId },
    //   relations: user,
    // });
    return await this.resume
      .createQueryBuilder('resume')
      .leftJoinAndSelect('resume.field', 'field')
      .where('resume.user = :user', { user: user.userId })
      .getMany();
  }
  async getListQuestion(resumeName, topic_question, inforResume) {
    const topic = await this.topicQuestion.findOne({
      topicQuestion: topic_question.topicQuestion,
    });
    // console.log(inforResume.fieldId);
    return await this.question
      .createQueryBuilder('question')
      .leftJoinAndSelect('question.field', 'field')
      .where('question.field = :field', { field: inforResume.fieldId })
      .andWhere('question.topicQuestion = :topicQuestion', {
        topicQuestion: topic.id,
      })
      .getMany();
  }

  async addOrUpdateAnswerQuestion(inforResume, listQuestion, action, answer) {
    const arrAnswer = answer.answerQuestion;
    if (arrAnswer.length != listQuestion.length) {
      return 'Please enter enough answers for each question';
    }
    const result = [];
    for (let i = 0; i < arrAnswer.length; i++) {
      const box = new Object();
      const answer = this.answerRepository.create({
        answerQuestion: arrAnswer[i].answerQuestion,
        question: listQuestion[i].id,
        resume: inforResume.id,
      });
      box['nameQuestion'] = listQuestion[i].contentQuestion;
      box['answerQuestion'] = arrAnswer[i].answerQuestion;
      result.push(box);
      await this.answerRepository.save(answer);
    }
    return result;
  }
}
